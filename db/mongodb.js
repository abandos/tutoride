var cfg          = require('config')
	, bdypar       = require('body-parser')
	, session      = require('cookie-session')
	, mongodb      = require('mongodb').MongoClient
	, envcfg       = require('./envcfg');

const drv = cfg.get('db');
const host = cfg.get(drv + '.location.host');
const port = cfg.get(drv + '.location.port');
const dbname = cfg.get(drv + '.location.db');
const admAuth = new Buffer(cfg.get('rest.environs.authorization'), "base64").toString();
const gpud = [ 'GET', 'PUT', 'DELETE' ];
const gpu = [ 'GET', 'PUT' ];
const gpod = [ 'GET', 'POST', 'DELETE' ];

// Recuerda actualizar environs después de modificar colección cfg
module.exports = function(app) {
	if(app) {
		// Need json parser for mongo
		app.use(bdypar.json());               
		app.use(bdypar.text({type:"text/plain"}));
	}
	this.driver = driver;
	// server access api
	this.getEnvConfig = getEnvConfig;
	this.getUserInfo = getUserInfo;
	this.getSessionInfo = getSessionInfo;
	this.changePass = changePass;
	this.getTemplates = getTemplates;

	return this;
}

function driver(req, res) {
	if(req.params.col === "_session" && gpod.indexOf(req.method) > -1)
		return eval(req.method.toLowerCase() + "Session(req, res)");
	if(req.params.col === "_users" && gpud.indexOf(req.method) > -1)
		return eval(req.method.toLowerCase() + "User(req, res)");
	if(req.params.col === cfg.get('rest.environs.db') && gpud.indexOf(req.method) > -1)
		return eval(req.method.toLowerCase() + "Config(req, res)");
	if(req.params.col === "templates" && gpu.indexOf(req.method) > -1)
		return eval(req.method.toLowerCase() + "Template(req, res)");
	if(req.params.col === "_config" && req.method === "PUT")
		return putAdminUser(req, res);
	log2all(res, { code:501, reason:"Method " + req.method + " for object '"
		+ req.params.col + "' not implemented" });
}

function getEnvConfig(cb) {
	dbConnect(admAuth).then(function(cli) {
		var db = cli.db(dbname);
		var col = db.collection(cfg.get('rest.environs.db'));
		col.find().toArray(function(err, docs) {
			cli.close();
			if(err) cb({ statusCode:500, body:err });
			cb({ statusCode:200, body:JSON.stringify(docs) });
		});
	});
}

function getUserInfo(email, name, cb) {
	dbConnect(admAuth).then(function(cli) {
		var db = cli.db(dbname);
		db.command({ usersInfo:email }, function(err, data) {
			cli.close();
			if(err) cb(err, null);
			else {
				if(data && data.users.length && data.users[0]
						&& data.users[0].customData && data.users[0].customData.fullname
						&& data.users[0].customData.fullname === name)
					cb(null, data);
				else
					cb({ error:400, reason:"MSG_SRV_USER_UNKNOWN" }, null);
			}
		});
	});
}

function getSessionInfo(req, cb) {
	if(!req.session)
		cb({ code:200, msg:'MSG_SRV_NO_SESSION' }, null);
	else
		cb(null, { ok:true, userCtx:{ name:req.session.user, roles:req.session.roles } });
}

function changePass(userdata, cb) {
	var username = userdata.users[0].user;
	var password = userdata.password;

	dbConnect(admAuth).then(function(cli) {
		var db = cli.db(dbname);
		db.command({ updateUser:username, pwd:password  }, function(err, data) {
			cli.close();
			if(err) cb({ error:401, reason:err.message }, null);
			else cb(null, { ok:true });
		});
	});
}

function getTemplates(env, req, cb) {
	dbConnect(req.session.user + ':' + req.session.password).then(function(cli) {
		var db = cli.db(dbname);
		var col = db.collection('templates');
		var key = req.params.env;
		col.find().toArray(function(err, docs) {
			cli.close();
			if(err) {
				console.error("getTemplates(ERR): " + err);
				cb({ code:500, msg:err }, null);
			}
			else {
				var templates = docs.filter(e => e._id === key)[0].templates;
				cb(null, templates);
			}
		});
	});
}

// Private functions

function dbConnect(usrpwd) {
	var url = "mongodb://" + usrpwd.replace('@','%40') + "@"
		+ host + ':' + port + '/' + dbname;

	return new Promise(function(resolve, reject) {
		mongodb.connect(url, function(err, cli) {
			if(err) reject(err);
			else resolve(cli);
		});
	});
}

function getSession(req, res) {
	if(!req.session)
		res.status(200).json({ code:200, userCtx:{ name:null } });
	else {
		req.session.renew = Math.floor(Date.now() / 60e3); // Renew session expiry
		res.json({ ok:true, userCtx: { name:req.session.user, roles:req.session.roles } });
	}
}

function postSession(req, res) {
	dbConnect(req.body.name + ':' + req.body.password).then(
		function(cli) {
			var db = cli.db(dbname);
			if(!req.session) {
				cli.close();
				return log2all(res, { code:500, reason:"MSG_SRV_ERR_MIDDLEWARE" });
			}
			db.command({ usersInfo:req.body.name }, function(err, data) {
				cli.close();
				if(err) return log2all(res, { code:500, reason:err });
				req.session.user = data.users[0].user;
				req.session.password = req.body.password;
				req.session.roles = data.users[0].roles.map(e => e.role);
				res.json({ ok:true, userCtx:{ name:req.session.user, roles:req.session.roles } });
			});
		},
		function(err) {
			log2all(res, { code:401, reason:err });
	});
}

function deleteSession(req, res) {
	req.session = null;
	res.status(200).json({ ok:true });
}

function getConfig(req, res) {
	var keys = req.query.keys ? JSON.parse(req.query.keys) : [ req.params.action ];
	var criteria = req.params.action === "_all_docs" && !req.query.keys ? {} : { _id:{ $in:keys } };

	dbConnect(req.session.user + ':' + req.session.password).then(
		function(cli) {
			var db = cli.db(dbname);
			var col = db.collection(cfg.get('rest.environs.db'));
			col.find(criteria).toArray(function(err, data) {
				cli.close();
				if(err) return res.status(500).send(err);
				if(req.params.action === "_all_docs") {
					var rows = data.map((e) => { return { "id":e._id, "doc":e } });
					res.json({ rows:rows });
				}
				else res.json(data[0]);
			});
	});
}

function putConfig(req, res) {
	dbConnect(req.session.user + ':' + req.session.password).then(function(cli) {
		var db = cli.db(dbname);
		var col = db.collection(cfg.get('rest.environs.db'));
		col.save(req.body, function(err, data) {
			cli.close();
			if(err) res.status(500).send(err);
			else {
				envcfg.load(function(err) { if(err) console.error(err); });
				res.json({ ok:true });
			}
		});
	});
}

function deleteConfig(req, res) {
	var action = req.params.action;

	dbConnect(req.session.user + ':' + req.session.password).then(function(cli) {
		var db = cli.db(dbname);
		var col = db.collection(cfg.get('rest.environs.db'));
		col.remove({ _id:action }, function(err, data) {
			cli.close();
			if(err) res.status(500).send(err);
			else {
				envcfg.load(function(err) { if(err) console.error(err); });
				res.json({ ok:true });
			}
		});
	});
}

function getTemplate(req, res) {
	var key = req.params.action;

	dbConnect(req.session.user + ':' + req.session.password).then(function(cli) {
		var db = cli.db(dbname);
		var col = db.collection('templates');
		col.find({ _id:key }).toArray(function(err, data) {
			cli.close();
			if(err) return res.status(500).send(err);
			return res.json(data[0]);
		});
	});
}

function putTemplate(req, res) {
	var template = req.body;

	dbConnect(req.session.user + ':' + req.session.password).then(function(cli) {
		var db = cli.db(dbname);
		var col = db.collection('templates');
		col.save(template, function(err, data) {
			cli.close();
			if(err) return res.status(500).send(err);
			else return res.json({ ok:true });
		});
	});
}

function getUser(req, res) {
	var username = req.params.action ? req.params.action.split(':')[1] : undefined;

	dbConnect(req.session.user + ':' + req.session.password).then(function(cli) {
		var db = cli.db(dbname);
		if(username) { // Request info of one user (user edit)
			db.command({ usersInfo:username }, function(err, data) {
				cli.close();
				if(err) return log2all(res, { code:500, reason:err });
				var userInfo = {
					"id":"org.couchdb.user:"+data.users[0].user,
					"name":data.users[0].user,
					"roles":data.users[0].roles.map(r => r.role),
					"envs":data.users[0].customData.envs,
					"fullname":data.users[0].customData.fullname,
					"derived_key":"dummypass"
				};
				res.json(userInfo);
			});
		}
		else { // Request all users (user management)
			db.command({ usersInfo:1 }, function(err, data) {
				cli.close();
				if(err) return log2all(res, { code:500, reason:err });
				var users = {
					"total_rows":data.users.length - 1,
					"offset":1,
					"rows":data.users
						.filter(u => u.user != "admin@tutoride.com")
						.map(function(u) { return { "id":u._id, "doc": { "name":u.user, "roles":u.roles.map(r => r.role), "envs":u.customData.envs, "fullname":u.customData.fullname, "derived_key":"dummypass" } } })
				};
				res.json(users);
			});
		}
	});
}

function putAdminUser(req, res) {
	var admin = req.params.admin;
	var password = req.body.replace(/"/g,"");

	dbConnect(req.session.user + ':' + req.session.password).then(function(cli) {
		var db = cli.db(dbname);
		db.command({ updateUser:admin, pwd:password }, function(err, data) {
			cli.close();
			if(err) return log2all(res, { code:500, reason:err });
			else res.json({ ok:1 });
		});
	});
}

function putUser(req, res) {
	var username = req.params.action ? req.params.action.split(':')[1] : undefined;
	var command = req.body.noCustomData ? {}
		: { customData:{ envs:req.body.envs, name:req.body.name, fullname:req.body.fullname } };

	if(req.body.password) command['pwd'] = req.body.password;
	dbConnect(req.session.user + ':' + req.session.password).then(function(cli) {
		var db = cli.db(dbname);
		db.command(Object.assign({ updateUser:username }, command), function(err, data) {
			if(err) {
				if(err.code !== 11) {
					cli.close();
					return log2all(res, { code:500, reason:err });
				}
				// UserNotFound: createUser
				db.command(Object.assign({ createUser:username, roles:req.body.roles }, command),
					function(err, data) {
						cli.close();
						if(err) log2all(res, { code:500, reason:err });
						else res.json({ ok:1 });
					}
				);
				return;
			}
			// Tutor users can't grant or revoke roles
			if(req.session.user !== "admin@tutoride.com") {
				cli.close();
				res.json({ ok:true });
				return;
			}
			db.command({ revokeRolesFromUser:username, roles:["learner", "tutor"] }, function(err, data) {
				if(err) log2all(res, { code:500, reason:err });
				else {
					if(req.body.roles.length) {
						db.command({ grantRolesToUser:username, roles:req.body.roles }, function(err, data) {
							if(err) log2all(res, { code:500, reason:err });
							cli.close();
							res.json({ ok:true });
						});
					}
					else {
						cli.close();
						res.json({ ok:true });
					}
				}
			});
		});
	});
}

function deleteUser(req, res) {
	var username = req.params.action ? req.params.action.split(':')[1] : undefined;

	dbConnect(req.session.user + ':' + req.session.password).then(function(cli) {
		var db = cli.db(dbname);
		db.command({ dropUser:username }, function(err, data) {
			cli.close();
			if(err) log2all(res, { code:500, reason:err });
			else res.json({ ok:true });
		});
	});
}

function log2all(res, msg) {
	var code = msg.reason && msg.reason.code ? msg.reason.code : msg.code;
	var reason = msg.reason && msg.reason.codeName ? msg.reason.codeName : msg.reason;

	res.status(msg.code).json({ error:code, reason:reason });
	console.error(reason);
}
