var cfg     = require('config')
	, request = require('request')
	, envcfg  = require('./envcfg')
	, hp      = require('http-proxy')

const db = cfg.get('db');
const proxy = hp.createProxyServer({ target:cfg.get(db + '.location') });
const confdb = "/" + cfg.get('rest.environs.db');
const host = cfg.get(db + '.location.host');
const port = cfg.get(db + '.location.port');

proxy.on('end', function(req, res) {
	if((req.method === 'PUT' || req.method === 'DELETE')
			&& req.url.startsWith(confdb))
		envcfg.load(function(err) { if(err) console.error(err); });
});

// restream parsed body before proxying
proxy.on('proxyReq', function(proxyReq, req, res, options) {
	if(req.body) {
		var bodyData = JSON.stringify(req.body);
		// incase if content-type is application/x-www-form-urlencoded -> we need to change to application/json
		proxyReq.setHeader("Content-Type", "application/json");
		proxyReq.setHeader("Content-Length", Buffer.byteLength(bodyData));
		// stream the content
		proxyReq.write(bodyData);
	}
});

module.exports = function(app) {
	this.driver = driver;
	// server access api
	this.getEnvConfig = getEnvConfig;
	this.getUserInfo = getUserInfo;
	this.getSessionInfo = getSessionInfo;
	this.changePass = changePass;
	this.getTemplates = getTemplates;
	return this;
};

function driver(req, res) {
	req.url = req.url.replace(cfg.get(db + ".part"), "");
	if(req.url === "/_session")
		return eval(req.method.toLowerCase() + "Session(req, res)");
	console.log("Proxying " + req.method + " to: " + req.url);
	var auth = new Buffer(req.session.user + ":" + req.session.password).toString("base64");
	req.headers["Authorization"] = "Basic " + auth;
	proxy.web(req, res);
}

// Public API

function getEnvConfig(callback) {
	var url ='http://' + cfg.get(db + '.location.host') + ':'
		+ cfg.get(db + '.location.port') + '/'
		+ confdb + '/_all_docs?include_docs=true';
	//console.log("Connecting to CouchDB at:", url);
	var authorization = cfg.get('rest.environs.authorization');

	request(url, { headers: { authorization: 'Basic ' + authorization } },
		function(err, res, body) {
			if(res.statusCode !== 200) callback(res);
			else {
				var rows = JSON.parse(res.body).rows.map((e) => e.doc);
				callback({ statusCode:200, body:JSON.stringify(rows) });
			}
		}
	);
}

function getUserInfo(email, name, cb) {
	var url = 'http://' + host + ':' + port + cfg.get('rest.usrinfo') + email;
	var authorization = cfg.get('rest.environs.authorization');
	request(url, { headers: { authorization: 'Basic ' + authorization } },
		function(err, res, body) {
			if(err || res.statusCode !== 200) {
				console.error("getUserInfo(ERR): " + (err || res.statusCode));
				cb({ error:res.statusCode, reason:"MSG_SRV_ERR_GET_INFO_USER" }, null);
			}
			else {
				var userdata = JSON.parse(body);
				if(userdata.fullname !== name)
					cb({ error:400, reason:"MSG_SRV_USER_UNKNOWN" }, null);
				else
					cb(null, userdata);
			}
	});
}

function getSessionInfo(req, cb) {
	if(!req.session || !req.session.user || !req.session.password)
		return cb({ code:401, msg:'Session expired. Please, sign in' }, null);
	var url = 'http://' + host + ':' + port + "/_session";
	var authorization = new Buffer(req.session.user + ':' +
		req.session.password).toString("base64");

	request(url, { headers:{ authorization:'Basic ' + authorization } },
		function(err, res, body) {
			if(err || res.statusCode !== 200) {
				console.error("getSessionInfo(ERR): " + (err||res.body));
				cb({ code:res.statusCode, msg:'MSG_SRV_GET_INFO_SESSION' }, null);
			}
			else cb(null, JSON.parse(body));
		}
	);
}

function changePass(userdata, cb) {
	var url = 'http://' + host + ':' + port
		+ cfg.get('rest.usrinfo') + userdata.name;
	var authorization = cfg.get('rest.environs.authorization');

	request.put(url, { headers:{ authorization:'Basic ' + authorization }, json:userdata },
		function(err, res, body) {
			if(err || res.statusCode !== 201) {
				console.error("changePass(ERR): " + (err || res.statusCode));
				cb({ error:res.statusCode, reason:'MSG_SRV_ERR_CHANGE_PASS' }, null);
			}
			else cb(null, { ok:true });
		}
	);
}

function getTemplates(env, req, cb) {
	var url = 'http://' + host + ':' + port + '/' + cfg.get('rest.templates.db') + '/' + env;
	var auth = new Buffer(req.session.user + ':' + req.session.password).toString("base64");

	request(url, { headers:{ authorization:'Basic ' + auth } },
		function(err, res, body) {
			if(err || res.statusCode !== 200) {
				console.error("getTemplates(ERR): " + err);
				cb({ code:res.statusCode, msg:'MSG_SRV_ERR_GET_TEMPLATES' }, null);
			}
			else cb(null, res);
		}
	);
}

// Private functions

function getSession(req, res) {
	console.log("Not proxying GET for _session");
	if(!req.session)
		res.status(200).json({ code:200, userCtx:{ name:null } });
	else {
		req.session.renew = Math.floor(Date.now() / 60e3); // Renew session expiry
		res.json({ ok:true, userCtx: { name:req.session.user, roles:req.session.roles } });
	}
}

function postSession(req, res) {
	console.log("Not proxying POST for _session");
	var url = 'http://' + host + ':' + port + "/_session";
	console.log(req.body);
	var authorization = new Buffer(req.body.name + ':' + req.body.password).toString("base64");

	if(!req.session) return res.send({ code:500,
		reason:"MSG_SRV_ERR_MIDDLEWARE" });
	request(url, { headers:{ authorization: 'Basic ' + authorization }, json:req.body },
		function(err, response, data) {
			if(err) return res.json({ error:500, reason:err });
			req.session.user = data.userCtx.name;
			req.session.password = req.body.password;
			req.session.roles = data.userCtx.roles;
			res.send(data);
		}
	);
}

function deleteSession(req, res) {
	console.log("Not proxying DELETE for _session");
	req.session = null;
	res.status(200).send({ ok:true });
}
