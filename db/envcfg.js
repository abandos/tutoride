var cfg = require('config')
	, db  = require('../' + cfg.get(cfg.get('db') + '.driver'))();

// BEWARE: there is a circular dependency between db.driver and envcfg
// So, don't do module.exports = {}
// Do module.exports.<function> to add exports to partial resolution instead
module.exports.load =
	function(callback) {
		// Load environments global configuration
		db.getEnvConfig(function(res) {
			if(res.statusCode === 200) {
				setEnvirons(res);
				callback();
			}
			else {
				console.error("Failed to get environment configuration: "
					+ JSON.stringify(res));
				callback(res);
			}
		});
	};

function setEnvirons(response) {
	var rows = JSON.parse(response.body);
	//console.log(rows);
	global.environs = {};
	for(var e in rows) {
		environs[rows[e]._id] = rows[e];
		environs[rows[e]._id].shells = {};
		environs[rows[e]._id].imgDefs = [];
		for(var i in environs[rows[e]._id].interpreters) {
			var s = environs[rows[e]._id].interpreters[i];
			environs[rows[e]._id].shells[s.id] = {
				command:s.command, image:s.image, workdir:s.workdir, net:s.net
			};
			var imgDef = { image:s.image, workdir:s.workdir, net:s.net };
			if(-1 === environs[rows[e]._id].imgDefs.indexOf(imgDef))
				environs[rows[e]._id].imgDefs.push(imgDef);
		}
		delete environs[rows[e]._id].interpreters;
	}
	//console.dir(environs, { depth:null });
	console.log('Read ' + Object.keys(environs).length
		+ ' environment configuration/s');
}
