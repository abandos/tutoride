(function() {
	'use strict';

	angular.module('app-tide')
		.constant("languageKeys", ["es", "en"])
		.constant("aliasesLanguages", {"es-*": "es", "en-*": "en", "*": "es" })
		.constant("defaultLanguage", "es");;

	angular.module('app-tide')
		.config(function(localStorageServiceProvider) {
			localStorageServiceProvider.setPrefix('app-tide');
			localStorageServiceProvider.setStorageType('localStorage');
		});

	angular.module('app-tide')
		.config(['$locationProvider', function($locationProvider) {
			$locationProvider.hashPrefix('');
		}]);

	angular.module('app-tide')
		.config(['languageKeys', 'aliasesLanguages', 
						 'defaultLanguage', '$translateProvider', 
			function(languageKeys, aliasesLanguages, 
								defaultLanguage, $translateProvider) {
				$translateProvider.useStaticFilesLoader({
					prefix: 'i18n/',
					suffix: '.json'
				});
				$translateProvider.registerAvailableLanguageKeys(languageKeys,
					aliasesLanguages);
				$translateProvider.preferredLanguage(defaultLanguage);
				$translateProvider.useStorage('appStorage');
		}]);
})();
