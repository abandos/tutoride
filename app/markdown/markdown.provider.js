(function() {
	'use strict';

	angular.module('app.markdown')
		.provider('markdownConverter', markdownConverter);

	function markdownConverter() {
		let options = {};
		return {
			config: function(newOpts) {
				options = newOpts;
			},
			$get: function() {
				let converter =  new showdown.Converter(options);
        converter.setFlavor('github');
				return converter;
			}
		}
	}
})();
