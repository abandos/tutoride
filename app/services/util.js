(function() {
	'use strict';

	/**
	 * Util service
	 */
	angular.module('app.services')
		.service('Util', Util);

	function Util () {
		this.demailize = demailize;
		this.deimagelize = deimagelize;
		this.depunctualize = depunctualize;
		
		return this;

		function demailize(email) {
			return depunctualize(email, true);
		}

		function deimagelize(email, image) {
			return depunctualize(email + '--' + image);
		}

		function depunctualize(name, includedot) {
			var from = ['@',':','/'];
			var to = ['_at_','_colon_','_slash_'];
			if(includedot) { from.push('.'); to.push('_dot_') }
			return name.split('').map(l => -1 < from.indexOf(l)
					? to[from.indexOf(l)] : l).join('');
		}
	}
})();
