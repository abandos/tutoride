(function() {
	'use strict';

	angular.module('app.services')
		.factory('widgets', ['$translate', widgets]);

	function widgets($translate) {
		var service = {
			waiting: waiting,
			addNotify: addNotify,
			modalConfirm: modalConfirm
		};
		return service;

		/**
		 * Show the spinner
		 */
		function waiting(wait) {
			if(wait) {
				var loading = $("#loading");
				loading.css("position", "absolute");
				loading.css("top", 
					($(window).height() - 80)/ 2 + $(window).scrollTop() + "px");
				loading.css("left", 
					($(window).width() - 180) / 2 + $(window).scrollLeft() + "px");
				$('html').addClass('waiting');
				$('.spinner-wrapper').removeClass('hide');
				loading.removeClass('hide');
				setTimeout(function(){ waiting(false); }, 10000);
			} else {
				$('html').removeClass('waiting');
				$('.spinner-wrapper').addClass("hide");
				$('#loading').addClass('hide');
			}
		}
		/**
		 * Global notification for error/warning/info messages
		 **/
		function addNotify(message, type, r) {
			if(undefined !== r) {
				if(undefined !== r.error && undefined !== r.reason) {
					// JSON error/message pair
					message += "<br><b>" + r.error + ":</b> " + 
						$translate.instant(r.reason);
				} else {
					if(undefined !== r.data && undefined !== r.data.error &&
						 undefined !== r.data.reason) {
						// JSON error/message into data member
						message += "<br><b>" + r.data.error + ":</b> "
							+ $translate.instant(r.data.reason);
					} else {
						if(undefined !== r.status && undefined !== r.statusText) {
							if(undefined !== r.statusText.error && undefined !== r.statusText.reason) {
								// HTTP statusText is an object with error and reason properties
								message += "<br><b>" + r.status + ":</b> " 
									+ $translate.instant(r.statusText.reason);
							} else {
								// Plain HTTP status/statusText pair
								var title = r.statusText.match(/<title>(.*)<\/title>/);
								message += "<br><b>" 
									+ $translate.instant(null === title ? r.statusText : title[1]) 
									+ "</b>";
							}
						}
					}
				}
			}
			if($('#notice-message').html() !== message + "<br>")
				$('#notice-message').append(message + "<br>");
			$('#notice-dialog').removeClass(function (index, css) {
				return (css.match(/\alert-\S+/g) || []).join(' ');
			});
			$('#notice-dialog').addClass('alert-'+type);
			$('#notice-box').modal('show');
			waiting(false);
		}
		/**
		 * Global confirm modal window
		 */
		function modalConfirm(message, ok, cancel) {
			$('#confirm-message').html(message);
			$('#confirm-btn').bind('click', function() {
				$('#confirm-box').modal('hide');
				if(ok) ok();
				$('#confirm-btn').unbind('click');
				$('#confirm-cnl').unbind('click');
			});
			$('#confirm-cnl').bind('click', function() {
				$('#confirm-box').modal('hide');
				if(cancel) cancel();
				$('#confirm-btn').unbind('click');
				$('#confirm-cnl').unbind('click');
			});
			$('#confirm-box').modal('show');
		}
		// Main entry point
		$(document).ready (function() {
			// Clear notice-box every time is dimmissed
			$('#notice-box').on('hidden.bs.modal', function(e) {
				$('#notice-message').html("");
			});
		});
	}	
})();
