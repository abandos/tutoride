(function() {
	'use strict';

	angular
		.module('app.services')
		.factory('sessionExpired', ['$q', '$location', '$rootScope', 'widgets',
						 '$translate', SessionExpired])
		.config(["$httpProvider",
  		function($httpProvider) {
				$httpProvider.interceptors.push('sessionExpired');
			}
		]); 

	function SessionExpired ($q, $location, $rootScope, widgets, $translate) {
		var service = {
			request: request,
			responseError: responseError,
		};
		
		return service;
		
		function request (config) {
			var path = $location.url();
			var session = $rootScope.$session;
			var auth = $rootScope.$auth;

			if(config.url !== '/tutoride/db/_session' &&
					config.url !== '/tutoride/api/resetpass' &&
					(config.url.indexOf('/tutoride') !== -1 ||
					(path === '/console' && config.url.endsWith("console.html")))) {
				auth.check(function(data) {
					session.create(data.userCtx.name, data.userCtx.roles);
					if(!session.isAuthenticated()){
						session.destroy();
						$location.path('/login');
						$translate("MSG_SESSION_EXPIRED").then(function(msg) {
							widgets.addNotify(msg, "warning");
						});
					} else {
						if(path === '/users' && !session.isTutor()
								|| path === '/envs' && !session.isRoot()) {
							$location.path('/');
							$translate("MSG_FORBIDDEN").then(function(msg) {
								widgets.addNotify(msg, "danger");
							});
						}
					}
				}, 
				function(error) {
					session.destroy();
					$location.path('/login');
					$translate("MSG_ERROR").then(function(msg) {
						widgets.addNotify(msg, "warning", error);
					});
				});
			}

			return $q.when(config);
		}

		function responseError(response) {
			var session = $rootScope.$session;
			if(!session.isAuthenticated()){
				if(response.status === 503){
					$location.path('/login');
					$translate("MSG_ERROR").then(function(msg) {
						widgets.addNotify(msg, "danger");
					});
				}
			}
			return $q.reject(response);
		}		
	}
})();


