/* Configure app for routerProvider */
(function() {
	'use strict';

	angular.module('app-tide')
		.config(['$routeProvider', config]);

	angular.module('app-tide')	
		.run(['$rootScope', '$route', runApp]);

	function runApp( $rootScope, $route) {
  	$rootScope.$on('$routeChangeSuccess', function(rcs) {
    	$rootScope.title = $route.current.title;
  	});
	}

  function config ($routeProvider) {
    $routeProvider.
      when("/about", {
        templateUrl: "layout/about.html",
        title: "¿Qué es TutorIDE?"
      }).
      when("/change-pass", {
        templateUrl: "session/change-pass.html",
        title: "Cambiar contraseña"
      }).
      when("/console", {
        templateUrl: "console/console.html",
        title: "Consola"
      }).
      when("/forgot", {
        templateUrl: "session/forgot.html",
        title: "Restablecer contraseña"
      }).
      when("/login", {
        templateUrl: "session/login.html",
        title: "Iniciar sesión"
      }).
      when("/sitemap", {
        templateUrl: "layout/sitemap.html",
        title: "Mapa del sitio"
      }).
      when("/users", {
        templateUrl: "users/users.html",
        title: "Gestión de usuarios"
      }).
      when("/envs", {
        templateUrl: "envs/envs.html",
        title: "Gestión de entornos"
      }).
      otherwise({
        redirectTo: "/about"
      });
	}
})();
