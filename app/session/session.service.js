(function() {
	'use strict';

	angular
		.module('app.session')
		.service('Session', Session);

	/**
	 * Session service
	 */
	function Session(localStorageService) {
	 	this.username = null;    // username 
    this.email = null;       // email user
    this.root = false;       // true is user admin
    this.tutor = false;      // true is tutor role
    this.database = null;    // user database
    this.envs = [];          // environment ids list
    this.activeEnv = null;   // Active environments
    this.activeInterp = null;// Active interpreters
		this.storage = localStorageService;
		// Array of validation function before the environment change
		this.onBeforeChangeEnv = [];
    //
    this.create = create;
    this.destroy = destroy;
    this.isAuthenticated = isAuthenticated;
    this.isTutor = isTutor;
    this.isRoot = isRoot;
    this.getImageDefs = getImageDefs;
		this.changeEnv= changeEnv;

		return this;

		/**
 		 * Create sessión
 		 * @param email email address
		 * @param roles list of roles associated with the user
		 **/
		function create(email, roles) {
			this.email = email;
			this.root = roles == null ? false :
   	   (email === "admin@tutoride.com"? true : false);
			this.tutor = this.root || (roles == null ? false : 
				(roles.indexOf('tutor') != -1 ? true : false));
			if(email != null) {
				this.database = email.replace("@", "_at_");
				this.username = email.split("@")[0];
			}
		}
		/**
 		 * Destroy session
 		 **/ 	
		function destroy () {
			this.email = null;
			this.root = false;
			this.tutor = false;
			this.database = null;
			this.envs = [];
			this.activeEnv = null;
			this.activeInterp = null;
		}

		function isAuthenticated () {
			return !!this.email;
		}
	
		function isTutor() {
			return this.tutor;
		}

		function isRoot () {
			return this.root;
		}

		function changeEnv(env) {
			this.activeEnv = env;
			this.activeInterp = env ? env.doc.interpreters[0] : undefined;
			if(env) {
				this.storage.set('activeEnvId', this.activeEnv.id);
				this.storage.set('activeInterpId', this.activeInterp.id);
			}
			else {
				this.storage.remove('activeEnvId');
				this.storage.remove('activeInterpId');
			}
		}
	
		function getImageDefs(userEnvs) {
			var imageDefs = [];
			var userEnvs = userEnvs || [];
			for(var e in this.envs) {
				if(-1 < userEnvs.indexOf(this.envs[e].id)) {
					for(var i in this.envs[e].doc.interpreters) {
						var image = this.envs[e].doc.interpreters[i].image;
						var workdir = this.envs[e].doc.interpreters[i].workdir;
						var imageDef = { image:image, workdir:workdir };
						if(-1 === imageDefs.indexOf(imageDef)) imageDefs.push(imageDef);
					}
				}
			}
			return imageDefs;
		}
	}
})();
