(function() {
	'use strict';

	angular
		.module('app.session')
		.factory('AuthService', ['$http', 'User', 'Env', '$translate', AuthService]);

	/**
	 * Authentication service
	 */
	function AuthService($http, User, Env, $translate) {
		this.login = login;
		this.logout = logout;
		this.check = check;

		return this;

		function login(user, password, success, error) {
			$http.post('/tutoride/db/_session',
				'{ "name":"' + user + '", "password":"' + password + '" }', {
					headers: { "Content-Type": "application/json" }
				}).then(function onSuccess(response) {
					var data = response.data;
					var status = response.status;
					var res = { name:data.userCtx.name, roles:data.userCtx.roles };
					if(typeof success === 'function') success(res, status);
				}, function onError(response) {
					if(typeof error === 'function') error(response.data, response.status);
				});
		}

		function logout(success, error) {
			$http.delete('/tutoride/db/_session')
				.then(function onSuccess(response) {
					if(typeof success === 'function') success(response.data, response.status);
				}, function onError(response) {
					if(typeof error === 'function') error(response.data, response.status);
				});
		}

		function check(success, error) {
			$http.get('/tutoride/db/_session')
				.then(function onSuccess(response) {
					if(typeof success === 'function') success(response.data, response.status);
				}, function onError(response) {
					if(typeof error === 'function') error(response.data, response.status);
				});
		}
  }
})();
