(function() {
	'use strict';
	
	angular.module('app.session')
		.factory('appStorage', ['Session', 'localStorageService', appStorage]);

	function appStorage(Session, localStorageService) {
		var service = {
			put: put,
			get: get,
			set: set,
			remove: remove,
			clearAll: clearAll,
			keys: keys
		};
		return service;

		/**
 		 * 
 		 * @param name
		 * @param value
		 **/
		function put(name, value) {
			localStorageService.remove(_prefix()+ name);
			localStorageService.set(_prefix() + name, JSON.stringify(value));
		}

		/**
 		 * 
 		 * @param name
		 * @return value
		 **/
		function get(name) {
			var value = localStorageService.get(_prefix() + name);
			if(name === "NG_TRANSLATE_LANG_KEY" && (!value || value === null)) {
				put(name, window.navigator.language ||
									window.navigator.userLanguage||
									window.navigator.languages[0] ||
									window.navigator.browserLanguage);
				value = localStorageService.get(_prefix() + name, value);
			}
			return JSON.parse(value);
		}

		/**
 		 * 
 		 * @param name
		 * @param value
		 **/
		function set(name, value) {
			put(name, value);
		}

		/**
 		 * 
 		 * @param name
		 **/
		function remove(name) {
			localStorageService.remove(_prefix() + name);
		}

		/**
 		 * Remove all data for this app from local storage.
 		 **/
		function clearAll() {
			localStorageService.clearAll();
		}

		/**
     * Return array of keys for local storage, ignore keys that not owned.
     * @return keys for local storage
     **/ 
		function keys() {
			return localStorageService.keys()
		}

		function _prefix() {
			return Session.isAuthenticated()? Session.username + ".": '';
		}
	}
})();
