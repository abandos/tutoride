(function() {
	'use strict';
	
	angular
		.module('app.session')
		.controller('SessionCtrl', ['$rootScope', '$location', '$http', 
			'AuthService', 'User', 'Session', 'Shell', 'Env', 'widgets', 
			'appStorage', '$translate', SessionCtrl]);

	function SessionCtrl($rootScope, $location, $http, AuthService, 
		User, Session, Shell, Env, widgets, appStorage, $translate) {
		var vm = this;
		vm.session = null;

		// Public API
		vm.login = login;
		vm.logout = logout;
		vm.changepass = changepass;
		vm.resetpass = resetpass;
		vm.changeEnv = changeEnv;

		activate();

		function activate() {
			if($rootScope.$session) {
				vm.session = $rootScope.$session;
			} else {
				vm.session = Session;
				$rootScope.$session = vm.session;
			}
			if(!$rootScope.$auth)
				$rootScope.$auth = AuthService;
			vm.session.storage = appStorage;
			$rootScope.$watch("$session.isAuthenticated()", function(nValue, oValue) {
				if(nValue) getUserEnvs();
			}, true);
			$rootScope.$watch("$session.envs", function(nValue, oValue) {
				if(nValue === oValue) return;
				if(oValue.length === 0 && nValue.length === 1) {
					vm.changeEnv(nValue[0], true);
					return;
				}
				if(nValue.length < oValue.length && vm.session.activeEnv) {
					// Environments deleted, check if it is the active environment
					var envs = oValue.filter(x => !nValue.includes(x));
					var activeEnv = envs.find(v => v == vm.session.activeEnv.id);
					if(activeEnv) 
						if(nValue.length > 0) vm.changeEnv(nValue[0], true);
						else {
							vm.session.changeEnv(undefined);
							widgets.addNotify($translate.instant("MSG_ADMIN_NOENVS"),
								"warning");
						}
				}
			}, true);
		}

		function login(form) {
			AuthService.login(form.email, form.password,
				function(data) {
					vm.session.create(form.email, data.roles);
					$location.path('/console');
				},
				function(error, status) {
					if(status !== 401)
						AuthService.logout();
					vm.session.destroy();
					if(status === 401) 
						widgets.addNotify($translate.instant("MSG_LOGIN_FAILED"), "danger")
					else
						widgets.addNotify($translate.instant("MSG_LOGIN_ERROR"), "danger", 
							{ status: status, 
								statusText: error });
				}
			);
		}

		function logout() {
			AuthService.logout(function(data, status){
				vm.session.destroy();
				$location.path('/about');
			},
			function(error, status) {
				vm.session.destroy();
				$location.path('/about');
			});
		}

		function changepass(form) {
			var newPass = form.newpass.trim().toString();
			var confPass = form.confpass.trim().toString();
			var email = vm.session.email;
			var raiserr = function(r) {
				widgets.addNotify($translate.instant("MSG_PASS_CHANGE_ERROR"), 
					"danger", r);
			};
			var raiseok = function(r, h) {
				// Transparent login: change password invalidates session cookie
				AuthService.login(email, newPass, function(data) {
					vm.session.create(email, data.roles);
					$location.path("/console");
					widgets.addNotify($translate.instant("MSG_PASS_CHANGE_SUCCESS"),
						"success");
				}, function() {
					vm.session.destroy();
					widgets.addNotify($translate.instant("MSG_PASS_CHANGE_SUCCESS_LOGIN"),
						"warning");
					$location.path("/login");
				});
			};

			if(newPass.localeCompare(confPass) != 0) {
				widgets.addNotify($translate.instant("MSG_PASS_DIFFERENT"),
					"danger");
			}
			else {
				if(vm.session.isRoot()) {
					$http.put("/tutoride/db/_config/admins/" + email,
						"\"" + newPass + "\"",
						{ headers:{ "Content-Type":"text/plain" } }).then(
					function(response){// Success
						raiseok(response);
					},
					function(response) {//Error
						raiserr(response);
					});
				} else {
					var user = User.get({id: "org.couchdb.user:" + email}, 
						function(user) {
							user.noCustomData = true;
							user.password =  newPass;
							user.$save(raiseok, raiserr);
						}, raiserr);
				}
			}
		}

		function resetpass(form) {
			let name = form.nombre.trim().toString();
			let email = form.email.trim().toString();
			let lang = $translate.use() || 
				Session.storage.get("NG_TRANSLATE_LANG_KEY");
			$("#bt").attr("disabled", true);
			$http.put("/tutoride/api/resetpass", { "email":email, "name":name, "lang":lang}).then(
				function(response){// Success
					widgets.addNotify($translate.instant("MSG_PASS_RESET_SUCCESS"),
						"success", response);
					$("#bt").attr("disabled", false);
				},
				function(response) {//Error
					widgets.addNotify($translate.instant("MSG_PASS_RESET_ERROR"),
						"danger", response);
					$("#bt").attr("disabled", false);
				});
		}
		/**
 		 * Change the active environment
 		 * @param envId environment identifier
 		 * @param noChangePath Indicate if yoy DO NOT have to change to the 
 		 *        console page. True no change page, false change to console page.
 		 */
		function changeEnv(envId, noChangePath) {
			if(vm.session.onBeforeChangeEnv.length > 0) {
				for(var i = 0; i < vm.session.onBeforeChangeEnv.length; i++) {
					var fn = vm.session.onBeforeChangeEnv[i];
					if(!fn()) return;
				}
			}
			// Gets envs data
			var r = getEnv(envId);
			r.$promise.then(function(dt) {
				if(!dt.rows || dt.rows[0].error) {
					widgets.addNotify($translate.instant("MSG_ENVID_GET_ERROR", 
						{"id": dt.rows[0].key}), "danger");
					return;
				}
				var activeEnv = dt.rows.find(v => v.id == envId);
				vm.session.changeEnv(activeEnv);
				if(noChangePath) return;
				if($location.path().indexOf("console") === -1)
					$location.path('/console');
			});
		}

		function getEnv(id) {
			return Env.get({_id: "_all_docs", keys: '["' + id + '"]', 
				include_docs: true});
		}

		function getUserEnvs() {
			var user = vm.session.email
				, isRoot = vm.session.isRoot();
			var result = (user === "admin@tutoride.com"?
				Env.query({_id: "_all_docs", include_docs: "true"}):
				User.get({id: "org.couchdb.user:" + user}));
			result.$promise.then(function(dt) {
				var envs = isRoot ? 
					dt.rows.map(function(v) { return v.id; }) :
					dt.envs;
				if(!envs || envs.length === 0) {
					if(isRoot) {
						widgets.addNotify($translate.instant("MSG_ADMIN_NOENVS"),
							"warning");
						$location.path('/envs');
					} else {
						vm.logout();					
						widgets.addNotify($translate.instant("MSG_USER_ENVS_NO_DEFINED"),
							"danger");
						return;
					}
				}
				// Set envs
				vm.session.envs = envs;	
				// Set activeEnv
				var activeEnvId = vm.session.storage.get('activeEnvId') || envs[0];
				activeEnvId = envs.find(v=> v === activeEnvId) || envs[0];
				var r = getEnv(activeEnvId);
				r.$promise.then(function(dt) {
					if(!isRoot && (!dt.rows || dt.rows.length === 0 || dt.rows[0].error)) {
						vm.logout();					
						widgets.addNotify($translate.instant("MSG_ENVID_GET_ERROR", 
							{"id": activeEnvId}), "danger");
						return;
					}
					var activeEnv = dt.rows[0];
					// Set activeInterp
					var activeInterpId = vm.session.storage.get('activeInterpId') ||
        		activeEnv.doc.interpreters[0].id;	
					vm.session.activeEnv = activeEnv,
					vm.session.activeInterp = activeEnv.doc.interpreters.find(
        		v => v.id === activeInterpId) || activeEnv.doc.interpreters[0];
					vm.session.storage.set('activeEnvId', activeEnv.id);
					vm.session.storage.set('activeInterpId', activeInterpId);
				}, function(error) {
					vm.logout();
					widgets.addNotify($translate.instant("MSG_ERROR"), "danger", error);
					return;
				});
			}, function(error) {
				vm.logout();
				widgets.addNotify($translate.instant("MSG_ERROR"), "danger", error);
				return;
			});
		}
	}
})();
