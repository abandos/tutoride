/* Define angular app and its dependencies */
(function() {
	'use strict';
	
	angular.module("app-tide", [
		"ngRoute",
		"LocalStorageModule",
		"pascalprecht.translate",
		"app.session",
		"app.services",
		"app.console", 
		"app.users",
		"app.envs"
	]);
})();
