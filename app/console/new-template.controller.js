(function(){
	'use strict';

	angular.module('app.console')
		.controller('NewTemplateCtrl', ['$scope', '$rootScope', 'Template', 
			'Console', 'widgets', '$translate', NewTemplateCtrl]);

	function NewTemplateCtrl($scope, $rootScope, Template, Console, widgets, $translate) {
		var vm = this;
		vm.newTemplate = {name: null, owner: null};
		vm.currentProject = null;
		// Public API
		vm.shareProject = shareProject;

		activate();

		function activate() {
      $scope.$watch('cc.Console.projectSelected', function(newValue, oldValue) {
        if(newValue != null && newValue.name === vm.currentProject) return
        vm.currentProject = (newValue == null? null: newValue.name);
      });
		}
	
		function shareProject() {
			var session = $rootScope.$session;
			var saveTemplate = function(tmpl, newSharedProject) {
				tmpl.templates[newSharedProject] = {
					interp: session.activeInterp.id, type: "live",
					owner: session.email, project: vm.currentProject};
				tmpl.$save({env: session.activeEnv.id, rev: tmpl._rev},
					function(d, h){
						widgets.addNotify($translate.instant("MSG_TEMPLATE_SHARED_SUCCESS",
							{"name": newSharedProject}), "success");
						Console.projectSelected.sharedTemplate = newSharedProject;
						Console.refreshTemplates(session.activeEnv.id, null,
							function(error) {
								widgets.addNotify($translate.instant("MSG_TEMPLATE_LOAD_ERROR"),
									"danger", error);
							});
						vm.newTemplate.name = null;
					}, function(err) {
						widgets.addNotify($translate.instant("MSG_TEMPLATE_SHARED_ERROR"),
							"danger", err);
					});
			};
			// Load templates document for the active env
			Template.get({ env:session.activeEnv.id}, 
				function(tmpl) {
					var templates = tmpl.templates;
					if(!templates[vm.newTemplate.name])
						return saveTemplate(tmpl, vm.newTemplate.name);
					if(templates[vm.newTemplate.name].owner == session.email)
						// User can overwrite the template, but issue a warning
						widgets.modalConfirm(
							$translate.instant("MSG_TEMPLATE_EXIST_CONFIR"),
							function() { saveTemplate(tmpl, vm.newTamplate.name)});
					else
						widgets.addNotify($translate.instant("MSG_TEMPLATE_EXIST", 
							{"owner": templates[vm.newTemplate.name].owner}), "danger", r);
				},
				function(r) {
					widgets.addNotify($translate.instant("MSG_TEMPLATE_LOAD_ERROR"),
						"danger", r);
				});
			$('#share-project').modal('hide');
		}
	}
})();
