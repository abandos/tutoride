(function() {
	'use strict';

	angular.module('app.console')
		.factory('Shell', ['$resource',
			function($resource) {
				return $resource('/tutoride/api/shell/:shell', { shell: "@shell" }, {
					execute: { method: 'POST' },
						query: { method: 'GET' }
				});
			}
		]);
})();
