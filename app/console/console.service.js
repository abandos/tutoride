(function() {
	'use strict';

	angular.module('app.console')
		.service('Console', ['$rootScope', 'Template', 'File', 
			'widgets', '$translate', Console]);

	function Console($rootScope, Template, File, widgets, $translate) {
		var vm = this;
		 /**
      * File list associated with a project. Structure of an element:
      * { name: filename, 
      *   content: content file, 
      *   active: true/false (activated tab),
      *   modified: true/false (if the content of the file has been modified),
      *   cursor: last cursor position in the editor }
      */
		vm.tabFiles = [];
		vm.projects = [];
		vm.projectSelected = null;
		vm.templates = [];
		vm.editor = null;
		vm.activateConsole = false;
		vm.selectedFolder = null;
		// Public API
		vm.init = init;
		vm.makeEditor = makeEditor;
		vm.sortNodes = sortNodes;
		vm.refreshTemplates = refreshTemplates;
		vm.changeProject = changeProject;
		vm.openFile = openFile;
		vm.isFileModified = isFileModified;
		vm.prefix = prefix;
		vm.storageOpenFiles = storageOpenFiles;
		vm.checkExtension = checkExtension;
		vm.saveFile = saveFile;
		return vm;

		// Functions
		/**
     * Initializes the objects associated with the context of the environment
     **/ 
		function init() {
			vm.tabFiles = [];
			vm.projects = [];
			vm.projectSelected = null;
			vm.templates = [];
			vm.activateConsole = false;
			vm.selectedFolder = null;
		}
		/**
     * Configure and initialize the file editor (Codemirror).
     * @param txtAreaElement textarea DOM will replace the CodeMirror
     * instance.
     **/ 
		function makeEditor(txtAreaElement) {
			vm.editor = CodeMirror.fromTextArea(txtAreaElement,
			{ lineNumbers: true,
				matchBrackets: true,
				autoCloseBrackets: true,
				extraKeys: {
					"Ctrl-S": function(instance) {
						var index = null;
						$.map(vm.tabFiles, function(t, i) {
							if(t.active){
								index = i;
								t.content = instance.getValue();
							}
						});
						if(index === null) return;
						vm.saveFile(index);
					}
				}
			});
			CodeMirror.modeURL = "../ext/codemirror/mode/%N/%N.js";
		}
		function saveFile(index) {
			if(index === undefined || index === null) return;
			if(vm.tabFiles[index].content === "") 
				vm.tabFiles[index].content === ' ';
			File.save({userdb: $rootScope.$session.database, 
								 path: vm.tabFiles[index].node.path,
								 env: $rootScope.$session.activeEnv.id, 
								 interp: $rootScope.$session.activeInterp.id, 
								 content: vm.tabFiles[index].content, isfile: true},
				function(data, status) {
					vm.tabFiles[index].modified = false;
					if(vm.tabFiles[index].name === 'macros.json') {
						vm.projectSelected.macros = vm.tabFiles[index].content;
					}
				}, function(r) {
					widgets.addNotify($translate.instant("MSG_FILE_SAVE"), 
						"danger", r);
				});
		}
		function sortNodes(n) {
			n.sort((a,b) => {
				if(Array.isArray(a.nodes)) {
					if(Array.isArray(b.nodes)) return a.title > b.title;
					else return -1;
				}
				else {
					if(Array.isArray(b.nodes)) return 1;
					else return a.title > b.title;
				}
			});
			n.forEach((e) => { if(e.nodes) sortNodes(e.nodes) });
		}
		function refreshTemplates(activeEnvId, fn_yes, fn_nno) {
			Template.query({env: activeEnvId},
				function(data, h) {
					vm.templates = data.templates ? Object.keys(data.templates) : [];
					for(var i in vm.projects) {
						var p = vm.projects[i];
						p.sharedTemplate = data.templates ? Object.keys(data.templates)
							.find(t => data.templates[t].type == "live"
							 && data.templates[t].project == p.name) : undefined;
					}
					if(fn_yes != null && fn_yes !== undefined)
						fn_yes(data, h);
				}, function(r) {
					if(fn_no != null && fn_no !== undefined)
						fn_no();
				}
			);
		}
		function changeProject(name) {
			vm.tabFiles.length = 0;
			vm.activateConsole = true;
			vm.projectSelected = vm.projects.find(
					e => e.name == name);
		}
		function changeCodeEditor(code, filename, scope) {
			var modeInfo = CodeMirror.findModeByFileName(filename);
			var changefun = function() {
    		$.map(vm.tabFiles, function(e,i){
					if(e.active && e.content !== vm.editor.getValue()){
						e.modified = true;
						e.content = vm.editor.getValue();
					}
				});
			 if(scope && !scope.$$phase) scope.$apply();
			}
			vm.editor.setOption("mode", modeInfo.mode);
			CodeMirror.autoLoadMode(vm.editor, modeInfo.mode);
			vm.editor.off("change", changefun);
			vm.editor.setValue(code);
			vm.editor.on("change", changefun);
		}
		function openFile(session, modelValue, fn_yes, scope) {
			var activeEditor = function(text) {
				if(!isTab)
					vm.tabFiles.slice(-1)[0].active = true;
				vm.storageOpenFiles(session, vm.tabFiles);
        changeCodeEditor(text, filename, scope);
				vm.activateConsole = false;
				fn_yes();
      };
			var isTab = false;
			var code = null;
			var filename = modelValue.title;
			var path = modelValue.path;

			// Save the contents of the previous file
      // Check if the file is already open
      $.map(vm.tabFiles, function(e, i) {
        if(e.active){
          e.content = vm.editor.getValue();
        }
        e.active = false;
        if(e.name === filename) {
          isTab = true;
          e.active = true;
          code = e.content;
        }
      });
     	if(!isTab) {
        File.get({ userdb: session.database, path: path,
                   env: session.activeEnv.id, interp: session.activeInterp.id,
                   isfile: true },
          function(data, status) { // success
            code = data.code;
            vm.tabFiles.push({ name: filename, content: code, active:false,
              modified:false, node: modelValue, cursor: null});
						if(fn_yes)
							activeEditor(code);
          },
          function(r) { // error
            widgets.addNotify($translate.instant("MSG_FILES_SHOW_ERROR"), 
							"danger", r);
          });
      } else {
				activeEditor(code);
      }
		}
		/**
     * Check if there are open files modified and not saved
     * @return true if there is any file modified and not saved, but false.
     **/
		function isFileModified(){
	    for(var i=0; i < vm.tabFiles.length; i++) {
				if(vm.tabFiles[i].modified) return true;
			}
			return false;
		}
	}
	/**
	 *
	 **/
	function prefix(session, key) {
		var activeEnv = session.activeEnv;
		return activeEnv? activeEnv.id + "." + key: key;
	}
	/**
	 *
	 **/
	function storageOpenFiles(session, files) {
		var of = files.map(function(v, i) {
			return v.name;
		});
		session.storage.set(prefix(session, "openFiles"), of);
	}
	/**
	 * Check if the extensión of the file matches any of the indicated
	 * extensions.
	 * @param filename name of file
	 * @param exts extensions array
	 * @return true if it matches any, otherwise false
	 **/
	function checkExtension(filename, exts) {
		var regex = /\.([0-9a-z]+$)/i;
		var result = regex.exec(filename);
		if(!result) return false;
		for(let i=0; i<exts.length; i++) {
			if(result.indexOf(exts[i]) != -1)	return true;
		}
		return false;
	}
})();
