(function() {
	'use strict';
	/**
 	 * Setting the options of the xeditable module
 	 * Create the CodeMirror Editor
 	 */ 
	angular.module('app.console').run(function(editableOptions){
  	editableOptions.theme = 'bs3';
	});
	/**
   * Configuring the options of the ui.tree module
   */ 
	angular.module('app.console').config(function(treeConfig) {
  	treeConfig.defaultCollapsed = true;
	});
	/**
   * Configuring the converter markdown
   **/
	
	angular.module('app.console').config(function(markdownConverterProvider) {
		markdownConverterProvider.config({
			extensions: ['mdtide'],
			tables: true,
			tasklists: true,
			ghCodeBlocks: false,
			excludeTrailingPunctuationFromURLs: true,
			literalMidWordUnderscores: true
		});
	});
})();
