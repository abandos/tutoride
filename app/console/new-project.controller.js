(function(){
	'use strict';

	angular.module('app.console')
		.controller('NewProjectCtrl', ['$scope', '$rootScope', 'Project', 
			'Console', 'widgets', '$translate', NewProjectCtrl]);

	function NewProjectCtrl($scope, $rootScope, Project, Console, widgets, $translate) {
		var vm = this;
		vm.newProject = {name: null, templateSelected: null};
		vm.templates = [];
		vm.projects = [];
		// Public API
		vm.addProject = addProject;

		activate();

		function activate() {
      $scope.$watch('cc.Console.projects', function(newValue, oldValue) {
        if(newValue === vm.projects) return
        vm.projects = newValue;
      });
      $scope.$watch('cc.Console.templates', function(newValue, oldValue) {
        if(newValue === vm.templates) return
				if(newValue && newValue.length) {
        	vm.templates = newValue;
					vm.newProject.templateSelected = vm.templates[0];
				}
      });
			
		}
		function addProject() {
			if(-1 !== vm.projects.findIndex(p => p.name == vm.newProject.name)){
      	widgets.addNotify($tranlate.instant("MSG_PROYECT_EXIST", 
					{"name": vm.newProject.name}), "danger"); 
      	return;
    	}
			var session = $rootScope.$session;
			// Add new project
			Project.save({userdb: session.database, proj:vm.newProject.name,
										env: session.activeEnv.id, interp: session.activeInterp.id,
										template: vm.newProject.templateSelected},
				function(data, statu) { // success
					// Add contents to project if exist
					var p = data;
					Console.projects.push({name: p.title, nodes: p.nodes, path: p.path,
						macros: p.marcos, readme: p.readme, sharedTemplate: undefined});
					Console.projects = Console.projects.sort((a,b) => a.name > b.name);
					setTimeout(function() {
						Console.changeProject(p.title);
						$('#add-project').modal('hide');
						$rootScope.$apply();
					}, 0);
				},
				function(error) { // error
					widgets.addNotify($translate.instant("MSG_PROYECT_ADD_ERROR"), 
						"danger", error);
				});
		}
	}
})();
