(function () {
	'use strict';
	
	angular.module('app.console')
		.controller('MarkdownCtrl', ['$scope', '$rootScope', '$compile', 
			'markdownConverter', MarkdownCtrl]);

	function MarkdownCtrl($scope, $rootScope, $compile, markdownConverter) {
		let vm = this;
		//
		vm.isMarkdown = isMarkdown;
		vm.classCode = classCode;
		//
		let file = undefined;
		
		activate();
		
		function activate() {
			file = undefined;
			//
			$scope.$watch('cc.Console.tabFiles', function (nValue, oValue) {
				vm.file = undefined;
				if(!nValue)	return; 
				nValue.forEach(function(e) {
					if(e.active) {
						file = e;
						makeHtml();
						return;
					}
				});
			}, true);
		}

		function isMarkdown() {
			if(!file) return false;
			let regex = /\.([0-9a-z]+$)/i;
			let result = regex.exec(file.name);
			if(!result) return false;
			if(result.indexOf("md") !== -1) return true;
			return false;
		}

		function classCode() {
			return vm.isMarkdown() ? "col-sm-6 col-md-6 col-lg-6" : "";	
		}

		function makeHtml() {
			let html = "";
			$('#markdown-text').html('');
			if(vm.isMarkdown()) {
				html = markdownConverter.makeHtml(file.content);
				$('#markdown-text').append(html)
				$compile($('#markdown-text'))($('#body').scope());
				$('#code-col').css("width", "50%");
			} else {
				$('#code-col').css("width", "100%");
			}
		}
	}
})();
