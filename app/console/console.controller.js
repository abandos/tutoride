(function() {
	'use strict';

	angular.module('app.console')
		.controller('ConsoleCtrl', ['$scope', '$rootScope', '$location', 
			'Console', 'Project', 'widgets', '$translate', ConsoleCtrl]);

	function ConsoleCtrl($scope, $rootScope, $location, Console, Project, 
		widgets, $translate) {
		var vm = this;
		vm.Console = Console;
		vm.storage = null;
		// Public API

		activate();

		function activate() {
			loadProjects($rootScope.$session);
			$rootScope.$session.onBeforeChangeEnv.push(
				function() {
				// Check if there are modules without saving before changing env.
				if(vm.Console.isFileModified()){
					widgets.addNotify($translate.instant("MSG_FILE_CHANGE"), "warning");
					return false;
				}
				return true;
			});
			// Checks if there are modules without saving before changing screen
			$rootScope.$on('$locationChangeStart', 
				function(event, newUrl, currentUrl){
					if(!$rootScope.$session.isAuthenticated()) return;
					if(currentUrl.indexOf('console') != -1){
						if(Console.isFileModified()){
							event.preventDefault();
							widgets.modalConfirm($translate.instant("MSG_FILE_CHANGE_CONFIR"),
								function() {
									for(var i=0; i < Console.tabFiles.length; i++)
										Console.tabFiles[i].modified=false;
									$location.url($location.url(newUrl).hash());
									$rootScope.$apply();
								}
							);
						}
					}
			});
			$scope.$watch('cc.Console.projectSelected', function(newValue, oldValue) {
				if(!newValue || newValue === oldValue) return;
				$rootScope.$session.storage.set(vm.Console.prefix($rootScope.$session,
					"projectSelected"), newValue.name);
			},true);
			$scope.$watch('$session.activeEnv', function(newValue, oldValue) {
				if(newValue == null || 
					 (oldValue != null && newValue.id === oldValue.id)) return;
				loadProjects($rootScope.$session);
			});
		}

		function loadProjects(session) {
			if(!session.database || !session.activeEnv) return;
			widgets.waiting(true);
			vm.Console.init();
			vm.Console.activateConsole = true;
	    Project.query({userdb: session.database, env: session.activeEnv.id,
        interp: session.activeInterp.id},
        function(data, h) { // success
          vm.Console.projects = [];
          vm.Console.projectSelected = null;
					vm.Console.tabFiles = [];
					vm.Console.templates = [];
          if(!session.activeEnv.id ||
            (data.title && data.title !== session.activeEnv.id)) return;
          for(var i in data.nodes) {
            var p = data.nodes[i];
            vm.Console.projects.push({name:p.title, nodes:p.nodes, path:p.path,
              macros:p.macros, readme:p.readme});
          }
          if(vm.Console.projects.length !== 0){
						var namePS = session.storage.get(Console.prefix(session, 
							"projectSelected"));
						var ps = vm.Console.projects.find(function(value, index) {
							if(value.name === namePS) return value;
						});
            vm.Console.projectSelected = ps || vm.Console.projects[0];
            vm.Console.sortNodes(vm.Console.projectSelected.nodes);
						if(ps) {
							var openFiles = session.storage.get(Console.prefix(session, 
								"openFiles"));
							if(openFiles) {
								openFiles.map(function(v, i) {
            			vm.Console.projectSelected.nodes.map(function(val, j) {
										if(val.title == v){
											vm.Console.openFile(session, val);
										};
									});
								});
							}
						}
          } else {
						session.storage.remove(Console.prefix(session, "projectSelected"));
						session.storage.remove(Console.prefix(session, "openFiles"));
					}
          vm.Console.refreshTemplates(session.activeEnv.id, null,
            function(error) {
              widgets.addNotify($translate.instant("MSG_TEMPLATE_LOAD_ERROR"),
                "danger", error);
          });
          $(".row").removeClass('hide');
          widgets.waiting(false);
          $('#divConsole').click();
        },
        function(error) {
          $(".row").removeClass('hide');
          widgets.waiting(false);
          widgets.addNotify($translate.instant("MSG_PROYECT_LOAD_ERROR"),
            "danger", error);
          $('#divConsole').click();
        }
      );
		}
	}
})();
