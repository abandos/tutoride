(function() {
	'use strict';

	angular.module('app.console')
		.factory('Project', ['$resource',
			function($resource) {
				return $resource('/tutoride/api/project/:env/:proj', 
					{ env:"@env", proj:"@proj", userdb:"@userdb", 
						interp:"@interp", template:"@template" },
					{ save: { method: 'PUT' },    // mkdir
				 	  remove: { method: 'DELETE' }, // rmdir
					  query: { method: 'GET' }     // dir
						//get: { method: 'GET' },
					}
				);
			}
		]);

	// TODO: Añadir soporte /archive/ de docker
	angular.module('app.console')
		.factory('File', ['$resource',
			function($resource) {
				return $resource('/tutoride/api/file/:env', 
					{ env:"@env", path:"@path", userdb:"@userdb", 
						interp:"@interp", isfile:"@isfile" }, 
					{ get: { method: 'GET', 
						transformResponse: function(data, headers) {
							return {'code': data} } 
						}
					}
				);
			}
		]);

	angular.module('app.console')
		.factory('Template', ['$resource',
			function($resource) {
				return $resource('/tutoride/db/templates/:env', 
					{ env:"@_id", rev:"@rev", include_docs:"@includeDocs"},
					{ get: { method: 'GET' }, 
					 query: { method: 'GET' }, 
					 save: { method: 'PUT' }
					}
				);
			}
		]);
})();
