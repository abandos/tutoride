(function() {
	'use strict';

	angular.module('app.console')
		.controller('NewElementCtrl', ['$scope', '$rootScope','Console', 
			'FileUploader', 'File', 'widgets', '$translate', NewElementCtrl]);

	function NewElementCtrl($scope, $rootScope, Console, FileUploader, File, widgets, $translate) {
		var vm = this;
		// Public API
		vm.newElement = {filename: null, foldername: null, section: 1};
		vm.uploader = new FileUploader({
			method: 'POST',
			removeAfterUpload: false
		});
		vm.selectedFolder = null;
		vm.folderOwner = null;
		vm.files = [];
		//
		vm.newFile = newFile;
		vm.changeSection = changeSection;
		vm.close = close;
		vm.removeFileUpload = removeFileUpload;
	
		activate();
		
		// List of uploader files erroneous
		var errorFilesUpload = [];

		function activate(){
			$scope.$watch('cc.Console.selectedFolder', function(newValue, oldValue) {
				if(!newValue || newValue === vm.selectedFolder) return;
				vm.selectedFolder = newValue;
				vm.folderOwner = newValue.name? $translate.instant('CNSL_ELMT_ROOT') :
					(newValue.title || newValue.name);
			}, true);
			$scope.$watch('cc.Console.projectSelected', function(newValue, oldValue) {
				if(newValue === vm.files) return;
				vm.files = (newValue == null? []: newValue.nodes);
			}, true);
			configureUploader();
		}	
		/**
     * Change the section identifier and send the focus to the DOM element
     * indicated.
     * @param section Section identifier
     * @param elem Element to send the focus
     **/ 	
		function changeSection(section, elem) {
			vm.newElement.section = section;
			setTimeout(function(){document.getElementById(elem).focus() }, 0);
		}
		/**
 		 * Close the window
 		 **/ 
		function close() {
			$('#add-file').modal('hide');
			vm.uploader.clearQueue();
			vm.newElement.filename = null;
			vm.newElement.foldername = null;
			vm.newElement.section = 1;
		}
		/**
     * Remove the file from the list of files to upload 
     * @param index indes of the file to be deleted
 		 **/
		function removeFileUpload (index) {
			vm.uploader.queue.splice(index,1);
		};
		/**
 		 * Create the new file o folder
 		 * @param isfile indicates whether the object to be created is a file or
 		 * a folder.
 		 **/
		function newFile(isfile) {
			var maxId = (n) => n.reduce((m, e) => Math.max(e.id, m,
      	e.nodes ? maxId(e.nodes) : 0), 0);
    	var filename = isfile ? vm.newElement.filename :
      	vm.newElement.foldername;
    	var exts = $rootScope.$session.activeEnv.doc.exts;
			// Check if the name of the file or folder is valid
			if(!checkFileName(isfile, filename, exts)) {
				widgets.addNotify($translate.instant("MSG_FILE_NAME_INVALID",
					{"exts": exts.join(', ')}), "danger");
      	return;
    	}
			// Check if the file or folder already in the list
			for(var f in vm.selectedFolder.nodes) {
				var name = vm.selectedFolder.nodes[f].title;	
				if( name === filename) {
					widgets.addNotify($tranlate.instant("MSG_ELEMENT_EXIST",
						{"name": name}), "danger");
					return;
				}
			}
			// Create the element
			var env = $rootScope.$session.activeEnv.id;
			var interp = $rootScope.$session.activeInterp.id;
			var db = $rootScope.$session.database;
			var path = vm.selectedFolder.path + '/' + filename;
			var node = {id: maxId(vm.files) + 1, path: path, title: filename};
			if(!isfile) node.nodes = [];
			File.save({userdb: db, path: path, env: env, interp: interp,
				content: " ", isfile: isfile}, 
				function(data, status) {
					vm.selectedFolder.nodes.push(node);
					Console.sortNodes(vm.selectedFolder.nodes);
					$('#add-file').modal('hide');
					vm.newElement.filename = null;
					vm.newElement.foldername = null;
				},
				function(r) {
					widgets.addNotify($translate.instant("MSG_ELEMENT_CREATE_ERROR"),
						"danger", r);
				}
			);
		} 
		// Private Function
		function checkFileName(isfile, filename, exts) {
			var re = /\.([A-Za-z0-9_-]+)$/;
			var rn = /^([A-Za-z0-9 _-]+)/;
			var specials = [ 'macros.json', 'readme.md', 'styles.css' ];

			var hasExt = re.test(filename);
			var hasValidExt = hasExt && -1 !== exts.indexOf(re.exec(filename)[1]);
			var hasValidName = rn.test(filename);
			var isSpecial = -1 !== specials.indexOf(filename.toLowerCase());
			var isTutor = $rootScope.$session.tutor;
			var isNotEmpty = filename !== undefined || filename !== null || 
				filename !== "";
			return isfile ? hasExt && hasValidExt && hasValidName && isNotEmpty || 
					(isTutor && isSpecial) : hasValidName && isNotEmpty;
		}
		/**
     *
     **/ 
		function configureUploader() {
			vm.uploader.filters.push({
    		name: 'FileFilter',
    		fn: function (item) {
      		var ext = /^.+\.([^.]+)$/.exec(item.name);
      		var exts = $rootScope.$session.activeEnv.doc.exts;
      		if(!checkFileName(true, item.name, exts)) {
        		widgets.addNotify($translate.instant("MSG_FILE_EXTENSION_INVALID",
							{"name": item.name, "exts": exts.join(', ')}), "danger");
       			return false;
      		}
      		// Check if the file already in the list
      		for(var f in vm.selectedFolder.nodes) {
        		var name = vm.selectedFolder.nodes[f].title;
        		if(name === item.name){
          		widgets.addNotify($translate.instant("MSG_FILE_UPLOAD_NO_ADD",
								{"name": name}), "warning");
            		return false;
          		break;
        		}
      		}
      		return true;
    		}
  		});
			vm.uploader.onBeforeUploadItem = function(item) {
				var env = $rootScope.$session.activeEnv.id;
				var interp = $rootScope.$session.activeInterp.id;
				var path = vm.selectedFolder.path + '/' + item.file.name;
				var userdb = $rootScope.$session.database;
				item.url = "/tutoride/api/file/" + env;
				item.url += "?interp=" + interp + "&isfile=true";
				item.url += "&path=" + path + "&userdb=" + userdb;
			};
			vm.uploader.onSuccessItem =function(item, response, status, headers) {
				var maxId = (n) => n.reduce((m, e) => Math.max(e.id, m,
					e.nodes ? maxId(e.nodes) : 0), 0);
				var path = vm.selectedFolder.path + '/' + item.file.name;
				var node = {id:maxId(vm.files) + 1,path:path,title:item.file.name};
				Console.selectedFolder.nodes.push(node);
				Console.sortNodes(vm.selectedFolder.nodes);
				item.remove();
			};
			vm.uploader.onErrorItem = function(item, response, status, headers) {
				errorFilesUpload.push(item.file.name);
			};
			vm.uploader.onCompleteAll = function() {
				if(errorFilesUpload.length){
					var msg = errorFilesUpload.length == 1? 
						$translate.instant("MSG_FILE_UPLOAD_ERROR"):
						$translate.instant("MSG_FILES_UPLOAD_ERROR", 
							{"files": errorFilesUpload.join('</i>, <i>')}); 
					widgets.addNotify(msg, "danger");
					errorFilesUpload = [];
				}
			};
		}	
	}
})();
