(function() {
	'use strict',

	angular.module('app.console')
		.controller('ListFilesCtrl', ['$scope', '$rootScope', 'File', 'Console', 
			'widgets', '$translate', ListFilesCtrl]);

	function ListFilesCtrl($scope, $rootScope, File, Console, widgets, $translate){
		var vm = this;
		vm.files = [];
		vm.tabFiles = [];
		vm.projects = [];
		vm.editor = null;
		vm.projectSelected = null;
		vm.activateConsole = false;

		// Public API
		vm.openFile = openFile;
		vm.deleteFile = deleteFile;
		vm.setFolder = setFolder;
	
		activate()

		function activate() {
			$scope.$watch('cc.Console.projects', function(newValue, oldValue) {
				if(newValue === vm.projects) return;
				vm.projects = newValue;
			}, true);
			$scope.$watch('cc.Console.projectSelected', function(newValue, oldValue) {
				if(vm.projectSelected === newValue) return;
				vm.projectSelected = newValue;
				vm.files = newValue == null? []: newValue.nodes;
			}, true);
			$scope.$watch('cc.Console.editor', function(newValue, oldValue) {
				if(newValue === vm.editor) return;
				vm.editor = newValue;
			});
			$scope.$watch('cc.Console.tabFiles', function(newValue, oldValue) {
				if(newValue === vm.tabFiles) return;
				vm.tabFiles = newValue;
			}, true);
			$scope.$watch('cc.Console.activateConsole', function(newValue, oldValue) {
				vm.activateConsole = newValue;
			}, true);
		}

		/**
 		 *
 		 */ 
		function openFile(scope) {
			var session = $rootScope.$session;
			var activeEditor = function() {
      	$("#code-container").show();
      	$("#tab-console").removeClass('active');
      	$("#console-container").hide();
				vm.editor.focus();
				vm.editor.refresh();
			};
			Console.openFile(session, scope.$modelValue, activeEditor, scope);
		}
		/**
 		 * Delete file or folder selected.
 		 **/
		function deleteFile(scope) {
			var db = $rootScope.$session.database;
			var env = $rootScope.$session.activeEnv.id;
			var interp = $rootScope.$session.activeInterp.id;
			var isfile = scope.$modelValue.nodes? false: true;
			var path = scope.$modelValue.path;
			var filename = scope.$modelValue.title;
			var what = $translate.instant(isfile? "MSG_FILE": "MSG_FOLDER");
			widgets.modalConfirm($translate.instant("MSG_FILE_DELETE", 
				{"what": what, "name": filename}),
				function() {
					File.delete({env: env, userdb: db, interp: interp, env: env,
						path: path, isfile: isfile}, 
						function(data, status){
							scope.remove();
							var index = null;
							$.map(vm.tabFiles, function(e, i) {
								if(e.name == filename)
									index = i;
							});
							if(index === null) return;
							if(vm.tabFiles[index].active)
								vm.activateConsole = true;
							vm.tabFiles.splice(index, 1);
						},
						function(error, status) {
							widgets.addNotity($translate.instant("MSG_FILE_DELETE_ERROR",
								{"what": what, "name": filename}), "danger", error);
						});
				}
			);
		}
		/**
		 * Set the value of the folder you have to create the new element.
		 * If you have not selected a folder, it will be created on the root of
		 * the project.
		 **/
		function setFolder(scope) {
			Console.selectedFolder = scope.$modelValue? 
				scope.$modelValue: vm.projectSelected;
		}	
	}
})();
