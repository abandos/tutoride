(function () {
	'use strict';

	angular.module('app.console')
		.controller('TutorConsoleCtrl', ['$scope', '$rootScope', 'Shell', 'File',
			'Console', 'UITutorConsole', '$translate', '$compile', 
			'markdownConverter', TutorConsoleCtrl]);

	function TutorConsoleCtrl($scope, $rootScope, Shell, File, Console, UITutorConsole, $translate, $compile, markdownConverter){
		var vm = this;
		vm.Shell = Shell;
		vm.File = File;
		vm.activeEnv = null;
		vm.database = null;
		vm.activeInterp = null;
		vm.resultid = 0;
		vm.projectSelected = null;
		vm.editor = undefined;
		vm.console = UITutorConsole;
		vm.options = {};
		vm.translate = $translate;
		vm.compile = $compile;
		vm.converter = markdownConverter;
		
		// Public API

		activate();

		function activate() {
			vm.activeEnv = $rootScope.$session.activeEnv;
			vm.database = $rootScope.$session.database;
			vm.activeInterp = $rootScope.$session.activeInterp;
			// Configure codemirror editor
			Console.makeEditor(document.getElementById("txtCode"));
			vm.editor = Console.editor;
			// Configure console options
			vm.options = {
				prompt: (vm.activeEnv ? vm.activeEnv.doc.prompt : ""),
				banner: '',
				cssClass:{ content: 'jquery-console-content',
										prompt: 'jquery-console-prompt-label',
									 cmdLine: 'jquery-console-input',
										parent: 'console, jquery-console'},
				commandConsole: { ':rendermd': rendermd },
				fnExecCommand: execCommand
			};
    	$('#console-container').click(function() {
      		vm.console.getInput().focus();
			});
			// watchs
			$scope.$watch('cc.Console.projectSelected', function(newValue, oldValue){
				if(newValue === vm.projectSelected) return;
				vm.projectSelected = newValue;
				if(!vm.projectSelected) return;
				// Search and show the file readme.md 
				let session = $rootScope.$session;
				let key = Console.prefix(session, vm.projectSelected.name + ".open");
				let node = vm.projectSelected.nodes.find(v => v.title === "readme.md"
					|| v.title === "README.md");
				vm.console.execCommand(":clear", [], false);
				if(node) {
					let isOpen = session.storage.get(key);
					if(isOpen) vm.console.execCommand(":rendermd", [node.title], false);
				}
				session.storage.set(key, "true");
			});
			$scope.$watch('$session.activeEnv', function(newValue, oldValue) {
				if (!newValue) return;
				vm.activeEnv = newValue;
				vm.console.setOptions({
					prompt: vm.activeEnv.doc.prompt
		 		});
			});
			$scope.$watch('$session.activeInterp', function(newValue, oldValue) {
				if (!newValue || newValue === oldValue) return;
				vm.activeInterp = newValue;
				vm.console.setOptions({
					currentInterpreter: vm.activeInterp.id,
					interpreters: getIdInterpreters(),
					commandAllowed: !vm.activeInterp.allowed ? 
						[] : vm.activeInterp.allowed,
					commandNotAllowed: !vm.activeInterp.notallowed ? 
						[]: vm.activeInterp.notallowed,
					macros: {}
				});
			});
			$scope.$watch('$session.database', function(newValue, oldValue) {
				if(newValue === vm.database) return;
				vm.database = newValue;
			});
			$scope.$watch('cc.Console.projectSelected.macros', 
				function(newValue, oldValue) {
					if(newValue === oldValue || vm.console === undefined) return;
					var p_macros = newValue;
					if(p_macros) {
						try {
							p_macros = JSON.parse(p_macros);
							vm.console.setOptions({ macros: p_macros });
						} catch(err) {
      				var reason = vm.translate.instant(
								"CNSL_MACROS_PARSE_ERROR", { 'what': err });
							var msg = '<span class="text-danger">' + reason + '</span>';
							vm.console.updateContent(null, msg);
							$("#console-container").scrollTop(
								$("#console-container")[0].scrollHeight);
							$("#tab-files").removeClass('working');
							Console.activateConsole = true;
						}
					}
			});
      $rootScope.$on('$translateChangeSuccess', function() {
				vm.console.setOptions({ language:$translate.use() });
      });
		}
		/**
 		 * Gets the list of interpreter identifiers
 		 * @return list of ids
 		 **/ 
		function getIdInterpreters() {
			if(!vm.activeEnv) return;
			var interps = [];
			if(vm.activeEnv.doc.interpreters !== undefined ||
				 vm.activeEnv.doc.interpreters.length !== 0) {
				for(var i in vm.activeEnv.doc.interpreters) {
					interps.push(vm.activeEnv.doc.interpreters[i].id);
				}
			}
			return interps;
		}
	}
  /**
   * Function that allows the execution of a command interpreter default
   * @param interp: interpret on which to execute the command.
   * @param    cmd: command to execute
   * @param tokens: parameter list
   **/
  function execCommand(interp, cmd, tokens) {
		var vm = $('#divConsole').scope().tcc;
		if(!tokens.includes(cmd)) tokens.unshift(cmd);
    var command = JSON.stringify(tokens.join(' '));
    $("#tab-files").addClass('working');
    sendCommand(vm, interp, command, function(data) {
      var output = data["cmdResult"],
         options = {canvas:{width:200, height:100}, node:{radius:8, sep:10}};
      vm.console.updateContent(tokens.join(' '),
        '<div class=\"ti-graph\" id=\"graph-' + (++vm.resultid) + '\"></div>' +
        '<div>' + (output ? '<pre>' + output + '</pre>' : '') + '</div>');
      if(data["cmdGraph"] != null) {
        if(data["csss"] != null) {
          var v = data["csss"].match(/\/\*!( *node=([0-9]*),([0-9]*) )?/);
          if(v && v[2] && v[3]) options.node = {radius:v[2], sep:v[3]};
        }
        var repType = data["cmdGraph"]["repType"] || "not present";
        switch(repType) {
          case "tree":
            addTree("#graph-" + vm.resultid, data["cmdGraph"],
                   data["csss"], options);
            break;
          case "not present":
          default:
            vm.console.update_content(tokens.join(' '),
              '<span class="text-danger">ERROR addGraph: `' + repType
              + '\'' 
							+ vm.translate.instant("MSG_CNSL_UNSUPPORTED_GRAPH")
							+ '</span>');
        }
      }
      $("#console-container").scrollTop(
        $("#console-container")[0].scrollHeight);
      $("#tab-files").removeClass('working');
    }, function(error, reason) {
      var msg = '<span class="text-danger">ERROR ' + error 
				+ (reason? ': ' + JSON.stringify(reason): '') + '</span>';
      vm.console.updateContent(tokens.join(' '), msg);
      $("#console-container").scrollTop(
        $("#console-container")[0].scrollHeight);
      $("#tab-files").removeClass('working');
    });
	}
  /**
   * Function that executes the command int the indicated interpreter 
   * @param      vm: controller scoped
   * @param   shell: interpret on which to execute the command.
   * @param command: command to execute
   * @param success: function to execute in case of success
   * @param   error: function to execute in case of error
   **/
  function sendCommand(vm, shell, command, success, error) {
		if(!vm.projectSelected) {
      error(vm.translate.instant("MSG_CNSL_PROJECT_UNDEFINED"));
      return;
    }
   	var currentProject = vm.projectSelected.name;

    if(!vm.activeEnv.doc){
      error(vm.translate.instant("MSG_CNSL_ENVS_UNDEFINED"));
      return;
    }
    var shell = vm.Shell.execute({shell: shell}, {
        command: command,
        project: currentProject,
        database: vm.database,
        env:vm.activeEnv.doc._id },
    function(r, h) {
      success(r);
    }, function(r) {
      error(r.data.error, r.data.reason);
    });
	}
	/**
   *
   **/
	function rendermd(_this) {
		var scope = $('#divConsole').scope();
		var vm = scope.tcc;
		if(!vm || !vm.activeEnv) return
		var filename = !_this || (Array.isArray(_this) && _this.length === 0) ?
			undefined : _this[0];
		var cmd = ":rendermd " + (!filename ? "": filename)
		if(!filename || filename.trim().length === 0) {
			vm.console.updateContent(cmd,
				vm.translate.instant("MSG_RENDERMD_NO_FILE")
			);
			return;
		}
		var exts = filename.match(/\.([0-9a-z]+$)/i);
		if(!exts || exts.indexOf("md") === -1) {
			vm.console.updateContent(cmd,
				vm.translate.instant("MSG_RENDERMD_NO_EXTENSION")
			);
			return;
		}
		var path = vm.projectSelected.path + '/' + filename
		vm.File.get({userdb: vm.database, path: path, env: vm.activeEnv.id,
							interp: vm.activeInterp.id, isfile: true },
			function(data, status) {
				var html = (!data.code || data.code.trim() === "" ?
					vm.translate.instant("MSG_RENDERMD_FILE_EMPTY", 
						{'filename': filename}) :
					 '<div class="tide-rm">'
					+ vm.converter.makeHtml(data.code)
					+ '</div>');
				vm.console.updateContent(cmd, html);
				vm.compile(vm.console.getContent())($('#body').scope());
			},
			function(error) {
				vm.console.updateContent(cmd, error);
			});
	}
})();
