(function() {
	'use strict';

	angular.module('app.console')
		.controller('ListProjectsCtrl', ['$scope', '$rootScope', 'Console', 
			'Project', 'Template', 'widgets', '$translate', ListProjectsCtrl]);

	function ListProjectsCtrl($scope, $rootScope, Console, Project, Template, widgets, $translate){
		var vm = this;
		vm.isSharedProject = false;
		vm.projects = [];
		vm.current = null;
		vm.tutor = false;
		vm.tabFiles = [];
		vm.shareProject = "share";

		// Public API
		vm.unShareProject = unShareProject;
		vm.changeProject = changeProject;
		vm.deleteProject = deleteProject;
	
		activate();

		function activate() {
			var session = $rootScope.$session;
			refreshSelectPicker();
			$rootScope.$on('$translateChangeSuccess', function() {
				refreshSelectPicker();
			});
			$rootScope.$watch('$session.tutor', function(newValue, oldValue) {
				vm.tutor = newValue;
			});
			$scope.$watch('$viewContentLoaded', function() {
				setTimeout(refreshSelectPicker, 1000);
			});
			$scope.$watch('cc.Console.projects', function(newValue, oldValue) {
				if(newValue === vm.projects) return
				vm.projects = newValue;
			});
			$scope.$watch('cc.Console.tabFiles', function(newValue, oldValue) {
				if(newValue === vm.tabFiles) return
				vm.tabFiles = newValue;
			},true);
			$scope.$watch('cc.Console.projectSelected', function(newValue, oldValue) {
				if(newValue == null || newValue === oldValue) return
				vm.current = newValue.name;
				vm.isSharedProject = newValue.sharedTemplate !== undefined;
				vm.shareProject = vm.isSharedProject ? "unshare" : "share";
			});
			$scope.$watch('cc.Console.projectSelected.sharedTemplate', 
				function(newValue, oldValue) {
					if(newValue === oldValue) return
					vm.isSharedProject = newValue !== undefined;
					vm.shareProject = vm.isSharedProject ? "unshare" : "share";
			});
		}
		function refreshSelectPicker() {
			$('select').selectpicker({ 
				noneSelectedText: $translate.instant('CNSL_PRJ_NOFOUND')});
			$('select').selectpicker('refresh');
		}
		function unShareProject() {
			var session = $rootScope.$session;
			Template.get({env:session.activeEnv.id}, function(tmpl) {
				var template = Console.projectSelected.sharedTemplate;
				widgets.modalConfirm($translate.instant("MSG_TEMPLATE_UNSHARE_CONFIR",
					{"name": template}), function() {
						delete tmpl.templates[template];
						tmpl.$save({ env:session.activeEnv.id, rev:tmpl._rev },
							function(d, h) {
								widgets.addNotify(
									$translate.instant("MSG_TEMPLATE_UNSHARE_SUCCESS", 
									{"name": template}), "success");
								Console.projectSelected.sharedTemplate = undefined;
								vm.isSharedProject = false;
								Console.refreshTemplates(session.activeEnv.id, null, 
									function(error) {
										widgets.addNotify(
											$translate.instant("MSG_TEMPLATE_LOAD_ERROR"), 
											"danger", error);
									}
								);
							}, function(err) {
								widgets.addNotify(
									$translate.instant("MSG_TEMPLATE_UNSHARE_ERROR"), 
									"danger", err);
							});
					});
				}
    	);
		}

		function pendingChangeModules(msg, fn_yes, fn_no) {
    	var pendingChanges = false;
    	for(var i=0; i < vm.tabFiles.length; i++) {
      	if(vm.tabFiles[i].modified){
        	widgets.modalConfirm(msg, fn_yes, fn_no);
        	return;
      	}
    	}
    	fn_yes();
  	}

		function changeProject() {
			var change = function() {
				Console.changeProject(vm.current);
			};
			var dontchange = function() {
				vm.current = Console.projectSelected.name;
				//Console.editor.focus();
			};
			var msg = $translate.instant("MGS_PROJECT_CHANGE_FILE_MODIF");
			pendingChangeModules(msg, change, dontchange);
		}
	
		function deleteProject() {
			if(vm.projects.length == 0) return;

			var proj = Console.projectSelected.name;
			var template = Console.projectSelected.sharedTemplate;
			var env = $rootScope.$session.activeEnv.id;
			var interp = $rootScope.$session.activeInterp.id;
			var database = $rootScope.$session.database;
			var msg = template? 
				$translate.instant("MSG_PROYECT_TEMPLATE_DELETE_CONFIR",
					{"project": proj, "template": template}): 
				$translate.instant("MSG_PROYECT_DELETE_CONFIR", {"project": proj});

			widgets.modalConfirm(msg, function() {
					Project.delete({userdb:database, env:env, interp:interp, proj:proj},
						function(data, status) {
							var ind = vm.projects.findIndex(e => e.name == proj);
							vm.projects.splice(ind, 1);
							setTimeout(function() {
								var next = vm.projects[ind-1] ||
									vm.projects[ind];
								Console.changeProject(next? next.name: null);
								if(!template) {
									Console.refreshTemplates(env);
									return;
								}
								Template.get({ env:env }, function(tmpl) {
              		delete tmpl.templates[template];
              		tmpl.$save({ env:env, rev:tmpl._rev },
                		function(d, h) {
											widgets.addNotify(
												$translate.instant("MSG_TEMPLATE_UNSHARE_SUCCESS", 
												{"name": template}), "success");
                  		Console.refreshTemplates(env, null, 
           							function(error) {
												widgets.addNotify(
													$translate.instant("MSG_TEMPLATE_LOAD_ERROR"), 
													"danger", error);
          							});
                		}, function(err) {
											widgets.addNotify(
												$translate.instant("MSG_TEMPLATE_UNSHARE_ERROR"), 
												"danger", err);
                		});
								});
							}, 0);
						}, 
						function(r) { // error
							widgets.addNotify($translate.instant("MSG_PROYECT_DELETE_ERROR"),
								"danger", r);
						}
					);
				});
		}
	}
})();
