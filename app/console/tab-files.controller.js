(function () {
	'use strict';

	angular.module('app.console')
		.controller('TabFilesCtrl', ['$scope', '$rootScope', 'File', 'Console', 
			'widgets', '$translate', TabFilesCtrl]);

	function TabFilesCtrl($scope, $rootScope, File, Console, widgets, $translate) {
		var vm = this;
		vm.tabFiles = [];
		vm.projects = [];
		vm.bannerEnv = null;
		// Public API
		vm.activateTabConsole = activateTabConsole;
		vm.activateTab = activateTab;
		vm.saveFile = Console.saveFile;
		vm.closeTab = closeTab;

		activate();
		
		function activate() {
			$scope.$watch('$session.activeEnv', function(newValue, oldValue) {
				if(!newValue) return;
				vm.bannerEnv = newValue.doc.banner;
			}, true);
			$scope.$watch('cc.Console.tabFiles', function(newValue, oldValue) {
				if(newValue === oldValue) return;
				vm.tabFiles = newValue;
				if(newValue.length === 0) 
					vm.activateTabConsole();
				// Check if any old element has been removed and was active
				for(var i in oldValue) {
					var isFound = false;
					for(var j in newValue){
						if(i === j) {
							isFound = true;
							break;	
						}
					}
					if(isFound === false && oldValue[i].active) {
						vm.activateTabConsole();
						break;
					}
				}
			}, true);
			$scope.$watch('cc.Console.projects', function(newValue, oldValue) {
				if(newValue === vm.projects) return;
				vm.projects = newValue;
			});
			$scope.$watch('cc.Console.activateConsole', function(newValue, oldValue) {
				if(newValue) {
					vm.activateTabConsole();
				}
			});
		}			
		function activateTabConsole() {
			Console.activateConsole = true;
			$.map(vm.tabFiles, function(t, i){
				t.active = false;
			});
			Console.editor.setValue("");
			$("#tab-console").addClass('active');
			$("#code-container").hide();
			$("#console-container").show();
			$('#divConsole').click();
		}

		function activateTab(index) {
    	var node = undefined;
			Console.activateConsole = false;
    	$.map(Console.projectSelected.nodes, function(f, i){
      	if(f.title === vm.tabFiles[index].name)
        	node = vm.tabFiles[index].node;
    	});
    	if(!node) return;
     	var session = $rootScope.$session;
      var activeEditor = function() {
        $("#code-container").show();
        $("#tab-console").removeClass('active');
        $("#console-container").hide();
  			Console.editor.focus();
      	Console.editor.refresh();
      };
    	Console.openFile(session, node, activeEditor, $scope);
		}
		function closeTab(index) {
			if(!vm.tabFiles[index].modified){
				vm.tabFiles.splice(index,1);
				Console.storageOpenFiles($rootScope.$session, vm.tabFiles);
				vm.activateTabConsole();
				return;
			}
   		widgets.modalConfirm($translate.instant("MSG_FILE_CLOSE_CONFIR"),
				function() {
					vm.tabFiles.splice(index,1);
					Console.storageOpenFiles($rootScope.$session, vm.tabFiles);
					vm.activateTabConsole();
				},
        function() {
        	Console.editor.focus();
        });
		}
	}
})(); 
