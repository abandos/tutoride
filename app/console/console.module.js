(function() {
	'use strict';

	angular.module('app.console', [
  	'xeditable',
  	'angularFileUpload',
  	'checklist-model',
  	'angular-bootstrap-select',
  	'ui.tree',
  	'angularResizable',
		'tutorConsole',
		'app.markdown'
	]);
})();
