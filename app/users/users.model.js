(function() {
	'use strict';

	angular
		.module('app.users')
		.factory('User', User);

	User.$inject = [ '$resource' ];
  function User($resource) {
    return $resource('/tutoride/db/_users/:id',
			{ id: "@id", rev: "@rev", include_docs: "@includeDocs", skip: "@skip" }, {
        get: { method: 'GET' },
      query: { method: 'GET' },
       save: { method: 'PUT' }
    });
  }
})();
