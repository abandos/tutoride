(function() {
	'use strict';

	angular.module('app.users', [
		'xeditable',
		'checklist-model',
		'ngResource'
	]);
})();
