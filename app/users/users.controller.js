(function() {
	'use strict'
	
	angular
		.module('app.users')
		.controller('UsersCtrl', [ '$rootScope', '$location', 'widgets', 'User', 
			'$translate', UsersCtrl ]);

  function UsersCtrl($rootScope, $location, widgets, User, $translate) {
		var vm = this;
		vm.add = false;
		vm.allroles = [ "learner", "tutor" ];
		vm.allenvs = [];
		vm.users = {};
		vm.inserted = false;

		// Public API
		vm.showEnvs = showEnvs;
		vm.checkName = checkName;
		vm.checkEmail = checkEmail;
		vm.checkPassword = checkPassword;
		vm.checkRoles = checkRoles;
		vm.showPassword = showPassword;
		vm.editUser = editUser;
		vm.createUser = createUser;
		vm.saveUser = saveUser
		vm.deleteUser = deleteUser;
		vm.cancelUser = cancelUser;
		vm.addUser = addUser;

		activate();

		function activate() {
			widgets.waiting(true);
			vm.users = User.query({ id:"_all_docs", include_docs:"true", skip:"1"},
				function(v, h) { // success
					vm.allenvs = $rootScope.$session.envs;
					$("#users").removeClass("hide");
					widgets.waiting(false);
				}, function(r) {
					$("#users").removeClass("hide");
					widgets.waiting(false);
					if($rootScope.$session.isTutor()) {
						widgets.addNotify($translate.instant("MSG_GETS_USERS"), 
							"danger", r);
					} else {
						$location.path('/console');
					}
				});
		}

		function showEnvs(userEnvs) {
			var selected = [];
			if(userEnvs === undefined || userEnvs === null)
				return '';
			angular.forEach(vm.allenvs, function(e) { 
				if (userEnvs.indexOf(e) >= 0) {
					selected.push(e);
				}
			});
			return selected.length? selected.join(', '): '';
		}

		function checkName(data) {
			if(data == null || data.trim() == "")
				return $translate.instant("MSG_FIELD_REQUIRED");
		}

		function checkEmail(data) {
			if(typeof(data) == "undefined")
				return $translate.instant("MSG_EMAIL_INVALID");
			if(data == null || data.trim() == "")
				return $translate.instant("MSG_FIELD_REQUIRED");
		}

		function checkPassword(data) {
			if(data == null || data.trim() == "")
				return $translate.instant("MSG_FIELD_REQUIRED");
		}

		function showPassword(data) {
			return "********";
		}

		function editUser(rowform) {
			rowform.$show();
			rowform.$editables[0].hide();
			rowform.$editables[1].inputEl.focus();
			if(!$rootScope.$session.isRoot()) {
				rowform.$editables[4].hide();
			}
		}

		function createUser(data) {
			var raiserr = function(r) {
				widgets.addNotify($translate.instant("MSG_ADD_ERROR",
					{'what': $translate.instant("USER_LABEL").toLowerCase()}), 
					"danger", r);
			};
			var user = new User({id: "org.couchdb.user:" + data.email});
			user.roles = data.roles || [] ;
			user.type = "user";
			user.name = data.email;
			user.fullname = data.name;
			user.password = data.password;
			user.envs = data.envs;
			user.$save(function(r) {
				widgets.addNotify($translate.instant("MSG_ADD_SUCCESS",
					{'what': $translate.instant("USER_LABEL")}), "success");
			}, function(r) {
				vm.users.rows.splice(0, 1);
				return raiserr(r);
			});
			vm.add = false;
		}

		function saveUser(data) {
			var raiserr = function(r) {
				widgets.addNotify($translate.instant("MSG_SAVE_ERROR",
					{'what': $translate.instant("USER_LABEL").toLowerCase()}), 
					"danger", r);
			};
			var user = User.get({id: "org.couchdb.user:" + data.email}, function(user) {
				user.roles = data.roles;
				user.fullname = data.name;
				user.password = data.password;
				user.envs = data.envs;
				if(undefined != user.derived_key && user.derived_key == data.password)
					delete(user.password);
				user.$save(function(r, headers) {
					widgets.addNotify($translate.instant("MSG_SAVE_SUCCESS",
						{'what': $translate.instant("USER_LABEL")}), "success");
				}, raiserr);
			}, raiserr);
		}

		function deleteUser(index, id) {
			var raiserr = function(r) {
				widgets.addNotify($translate.instant("MSG_DELETE_ERROR",
					{'what': $translate.instant("USER_LABEL").toLowerCase()}), 
					"danger", r);
			};
			var user = User.get({id: "org.couchdb.user:" + id}, function(user) {
				widgets.modalConfirm($translate.instant("MSG_USER_DELETE_CONFIR",
					{"id": id}), function() {
						user.$delete({id: "org.couchdb.user:" + id, rev: user._rev},
							function(r) {
								widgets.addNotify($translate.instant("MSG_DELETE_SUCCESS",
									{'what': $translate.instant("USER_LABEL")}), "success");
								vm.users.rows.splice(index, 1);
							}, function(r) {
								return raiserr(r);
							});
				});
			}, raiserr);
		}

		function cancelUser(index) {
			if(vm.add){
				vm.users.rows.splice(index, 1);
				vm.add = false;
			}
		}

		function addUser() {
			if(vm.add) return;
			vm.inserted = {
				doc:{
					name: null,
					email: null,
					password: null,
					envs: [],
					roles: []
				}
			};
			vm.users.rows.unshift(vm.inserted);
			vm.add = true;
		}
		function checkRoles(data) {
			if(!data || data.length === 0)
				return $translate.instant("MSG_ROLE_REQUIRED");
		}
	}
})();
