/**
 * @desc 
 * @example <a class="savetab" data-tide-tooltip="Guardar">Guardar</a>
 **/
(function() {
	'use strict';

	angular.module('app-tide')
		.directive('tideTooltip', ['$rootScope', '$translate', Tooltip]);

	function Tooltip($rootScope, $translate) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs){
				var translationChangeOcurred = function() {
					var t = $translate.instant(attrs.tideTooltip);
					element.attr("title",t);
					element.tooltip('fixTitle');
				};
				translationChangeOcurred();
				$rootScope.$on('$translateChangeSuccess', translationChangeOcurred);
			}
		};
	}
})();
