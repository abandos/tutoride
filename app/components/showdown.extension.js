(function (extension) {
  'use strict';
	if (typeof showdown !== 'undefined') {
    // global (browser or nodejs global)
    extension(showdown);
  } else if (typeof define === 'function' && define.amd) {
    // AMD
    define(['showdown'], extension);
  } else if (typeof exports === 'object') {
    // Node, CommonJS-like
    module.exports = extension(require('showdown'));
  } else {
    // showdown was not found so we throw
    throw Error('Could not find showdown library');
  }
}(function (showdown) {
  'use strict';
	showdown.extension('mdtide', function() {
		/**
     * Repalce the link section: [Section](#section) by
     * <a href="javascript:void(0)" data-ng-click="aptide.scrollTo('section')>
     * 	Section
     * </a>.
     **/
		var tableOfContents = { 
			type: 'lang',
			regex: /\[([^[]*)\]\(#([^\)]*)\)/g,
			replace: function(match, prefix, content) {
				var result = '<a href="javascript:void(0)"'
					+ ' data-ng-click="aptide.scrollTo(' 
					+ "'" + content.trim() + "'" + ')">'
					+ prefix + '</a>';
				return result;
			}
		};
		return [tableOfContents];
	});
}));
