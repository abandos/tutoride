/**
 * @desc directive that sends the focus of the cursor to the element that 
 * defines it
 * @example <input id="email" tide-autofocus />
 **/
(function () {
	'use strict';

	angular.module('app-tide')
		.directive('tideAutofocus', autoFocus);

	function autoFocus() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				if(!attrs.disabled){
					element.focus();
				}
			}
		};
	}
})();
