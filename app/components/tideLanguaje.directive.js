/**
 * @desc
 * @example
 **/
(function() {
	'use strict';
	
	angular.module('app-tide')
		.directive('tideLanguaje', ['$translate', TideLanguaje]);

	function TideLanguaje($translate) {
		return {
			retrict: 'A',
			link: function (scope, element, attrs) {
				var lang = attrs.tideLanguaje;
				element.bind('click', function() {
					$translate.use(lang);
					return false;
				});
			}
		};
	}
})();
