(function() {
	'use strict';
	angular.module('app-tide')
		.controller('TideCtrl', ['$rootScope', '$translate', '$route',
			'$location', '$anchorScroll',
			'Session', 'AuthService', 'defaultLanguage', TideCtrl]);

	function TideCtrl($rootScope, $translate, $route, $location, $anchorScroll, Session, AuthService, defaultLanguage) {
		var vm = this;
		vm.session = Session;
		vm.currentLang = null;
		vm.route = $route;
		vm.aboutUrl = aboutUrl;
		vm.scrollTo = scrollTo;
		
		activate();

		function activate() {	
			if(!$rootScope.$auth)
				$rootScope.$auth = AuthService;
			$rootScope.$watch('$session', function(newValue, oldValue) {
				if(newValue === oldValue) return;
				vm.session = newValue;
				$translate.use(vm.session.storage.get("NG_TRANSLATE_LANG_KEY") ||
					defaultLanguage);
			}, true);
			$rootScope.$on('$translateChangeSuccess', function() {
				vm.currentLang = $translate.use().toUpperCase();
			});
			$rootScope.$on('$translateChangeError', function (event, a) {
				$translate.use(defaultLanguage);
			});
			$rootScope.$on('$translateLoadingError', function (event, a) {
				$translate.use(defaultLanguage);
			});
		}
		/**
     *
     **/ 
		function aboutUrl() {
			var lang = vm.currentLang? vm.currentLang: defaultLanguage;
			return 'layout/about_' + lang.toLowerCase() + ".html";
		}
		/**
     *
     **/ 
		function scrollTo(id) {
			$anchorScroll(id);
		}
	}
})();
