(function() {
	'use strict'
	
	angular
		.module('app.envs')
		.controller('EnvsCtrl', [ '$location', 'widgets', 'Env', 'Container', 
			'Net', '$translate', '$rootScope', EnvsCtrl ]);

		function EnvsCtrl($location, widgets, Env, Container, Net, $translate, $rootScope) {
			var vm = this;
			vm.add = false;
			vm.allimages = [];
			vm.allnets = [];
			vm.introws = [];
			
			// Public API
			vm.required = required;
			vm.editEnv = editEnv;
			vm.obsInterp = obsInterp;
			vm.createEnv = createEnv;
			vm.saveEnv = saveEnv;
			vm.deleteEnv = deleteEnv;
			vm.cancelEnv = cancelEnv;
			vm.addEnv = addEnv;
		
			activate();

			function activate() {
				widgets.waiting(true);
				// Load images by calling to docker service: /images/json
				Container.list({}, function(v, h) { // success
					vm.allimages = v.rows;
				}, function(r) { //error
					widgets.addNotify($translate.instant("MSG_GETS_DOCKER_IMAGES"), 
						"danger", r);
				});
				// Load nets by calling to docker service: /nets/json
				Net.list({}, function(v, h) { // success
					vm.allnets = v.rows;
				}, function(r) { //error
					widgets.addNotify($translate.instant("MSG_GETS_DOCKER_NET"), 
						"danger", r);
				});
				vm.envs = Env.query({_id: "_all_docs", include_docs: "true"}, function(v, h) {
					$("#envs").removeClass("hide");
					widgets.waiting(false);
				}, function(r) { // error
					$("#envs").removeClass("hide");
					widgets.waiting(false);
					if(vm.$root && vm.$root.$session.isRoot()){
						widgets.addNotify($translate.instant("MSG_GETS_ENVS"), "danger", r);
					} else {
						$location.path('/console');
					}
				});
			}

			function required(data) {
				if(!data || (typeof(data) === "string" &&  data.trim() == "")
						|| data.length && data.length === 0)
					return $translate.instant("MSG_FIELD_REQUIRED");
			}

			function editEnv(rowform) {
				rowform.$show();
				rowform.$editables[0].hide();
				rowform.$editables[1].inputEl.focus();
			}

			function obsInterp(i, index) {
				var data = i.$data;
				if(data == null || (typeof(data) == "string" && data.trim() == ""))
					return $translate.instant("MSG_FIELD_REQUIRED");
				if(!vm.introws[index]) vm.introws.push({});
				if(i.$editable.name == "i.id") vm.introws[index].id = data;
				if(i.$editable.name == "i.command") vm.introws[index].command = data;
				if(i.$editable.name == "i.image") vm.introws[index].image = data;
				if(i.$editable.name == "i.workdir") vm.introws[index].workdir = data;
				if(i.$editable.name == "i.net") vm.introws[index].net = data;
			}

			function createEnv(data, rowform) {
				var env = new Env({_id: data.id});
				var exts = data.exts && data.exts.length > 0 ? data.exts.split('\n'): undefined;
				var allowed = data.allowed && data.allowed.length > 0 ? data.allowed.split('\n'): undefined;

				env.allowed = allowed;
				env.exts = exts;
				env.banner = data.banner;
				env.prompt = data.prompt;
				env.ttl = data.ttl;
				env.interpreters = vm.introws;
				for(var i = 0; i < env.interpreters.length; i++) {
					var c = env.interpreters[i].command;
					env.interpreters[i].command = c.length > 0 ? c.split('\n') : [];
				}
				env.$save(function(r) {
					widgets.addNotify($translate.instant("MSG_ADD_SUCCESS",
						{'what': $translate.instant("ENVS_LABEL")}), "success");
					vm.add = false;
					// Add env in $rootScope.$session.envs
					var saved = { id:data.id, doc:{ _id:data.id, banner:data.banner, 
						allowed:allowed, exts:exts, interpreters:vm.introws, 
						prompt:data.prompt } };
					$rootScope.$session.envs.push(data.id);
				}, function(r) {
					widgets.addNotify($translate.instant("MSG_ADD_ERROR",
						{'what': $translate.instant("ENVS_LABEL").toLowerCase()}), 
						"danger", r);
					rowform.$show();
				});
			}

			function saveEnv(data, rowform) {
				var raiserr = function(r) {
					widgets.addNotify($translate.instant("MSG_SAVE_ERROR",
						{'what': $translate.instant("ENVS_LABEL").toLowerCase()}), 
						"danger", r);
					rowform.$show();
				};
				var env = Env.get({_id: data.id}, function(env) {
					var exts = data.exts && data.exts.length > 0 ? data.exts.split('\n'): undefined;
					var allowed = data.allowed && data.allowed.length > 0 ? data.allowed.split('\n'): undefined;
					env.banner = data.banner;
					env.prompt = data.prompt;
					env.ttl = data.ttl;
					env.interpreters = vm.introws;
					env.allowed = allowed;
					env.exts = exts;
					for(var i = 0; i < env.interpreters.length; i++) {
						var c = env.interpreters[i].command;
						env.interpreters[i].command = c.length > 0 ? c.split('\n') : [];
					}
					env.$save(function(r, headers) {
						widgets.addNotify($translate.instant("MSG_SAVE_SUCCESS",
							{'what': $translate.instant("ENVS_LABEL")}), "success");
						// Save env in $rootScope.$session.envs
						var saved = { id:data.id, doc:{ _id:data.id, banner:data.banner, allowed:allowed, exts:exts,
							interpreters:vm.introws, prompt:data.prompt } };
						$rootScope.$session.envs.forEach(function(o, i) {
							if(o === data.id) {
								if(data.id !== $rootScope.$session.activeEnv.id) return;
								$rootScope.$session.activeEnv = saved;
								$rootScope.$session.activeInterp = saved.doc.interpreters[0];
							}
						});
					}, raiserr);
				}, raiserr);
			}

			function deleteEnv(index, id) {
				var raiserr = function(r) {
					widgets.addNotify($translate.instant("MSG_DELETE_ERROR",
						{'what': $translate.instant("ENVS_LABEL").toLowerCase()}), 
						"danger", r);
				};
				var env = Env.get({_id: id}, function(env) {
					widgets.modalConfirm($translate.instant("MSG_ENVS_DELETE_CONFIR",
						{"id": id}), function() {
							env.$delete({_id: id, rev: env._rev},
								function(r) {
									vm.envs.rows.splice(index, 1);
									// Remove env from $rootScope.$session.envs
									var deleted = $rootScope.$session.envs.findIndex(o => o == id);
									if(-1 == deleted) return;
									$rootScope.$session.envs.splice(deleted, 1);
									widgets.addNotify($translate.instant("MSG_DELETE_SUCCESS",
										{'what': $translate.instant("ENVS_LABEL")}), "success");
								}, raiserr);
						});
				}, raiserr);
			}

			function cancelEnv(index) {
				if(!vm.add) return;
				vm.envs.rows.splice(index, 1);
				vm.add = false;
			}

			function addEnv() {
				if(vm.add) return;
				vm.inserted = {
					doc:{
						id: null,
						banner: null,
						prompt: null,
						ttl: null,
						interpreters: [{ id: null, command: null, image: null, net: null }],
						allowed: null,
						exts: null
					}
				};
				vm.envs.rows.unshift(vm.inserted);
				vm.add = true;
			}
	}
})();
