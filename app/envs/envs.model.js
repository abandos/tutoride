(function() {
	'use strict';

	angular
		.module('app.envs')
		.factory('Env', Env)
		.factory('Container', Container)
		.factory('Net', Net);

	Env.$inject = [ '$resource' ];
  function Env($resource) {
    return $resource('/tutoride/db/config/:_id', { _id: "@_id", rev: "@rev", include_docs: "@includeDocs" }, {
      	  get: { method: 'GET' },
      	query: { method: 'GET' },
      	 save: { method: 'PUT' }
  		});
	}

	Container.$inject = [ '$resource' ];
  function Container($resource) {
    return $resource('/tutoride/api/container/:cid', { cid: "@cid" }, {
				 list: { method: 'GET' },
			 create: { method: 'PUT' },
			 remove: { method: 'DELETE' }
    });
  }

	Net.$inject = [ '$resource' ];
  function Net($resource) {
    return $resource('/tutoride/api/net', {}, {
				 list: { method: 'GET' }
    });
  }
})();
