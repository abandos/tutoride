(function() {
	'use strict';

	angular.module('app.envs', [
		'xeditable',
		'ngResource'
	]);
})();
