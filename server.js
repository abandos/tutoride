var cfg       = require('config')
	, express   = require('express')
	, http      = require('http')
	, bdypar    = require('body-parser')
	, reload    = require('reload')
	, session   = require('cookie-session')
	, envcfg		= require('./db/envcfg');

// Express configuration
var app = express();                       // Use express for REST
app.use(session({													 // Use cookie session
	secret:cfg.get('cookie.secret'),
	maxAge:cfg.get('cookie.timeout')
}));
app.set('port', cfg.get('http.port'));     // Override port with env PORT 
app.set('host', cfg.get('http.host'));     // Override host with env PORT 

var db = cfg.get('db');                    // DB name: mongo or couch
var drv = require(cfg.get(db + '.driver'))(app);// Use DB driver in cfg
app.use(bdypar.json());                    // Need urlencoded parser for mongo
app.all(cfg.get(db + '.uri'), drv.driver); // Reg db API before any uses
app.use(express.static('app'));            // Every JS, HTML under app is static
app.use(cfg.get('http.uri') + '/api', require('./rest')); // REST api endpoint

app.get('/', function(req, res) {          // Angular single page index
	res.sendFile(__dirname + '/app/layout/tide.html');
});
app.use(function(err, req, res, next) {    // Default error handler
	console.error("ERROR: " + err.message||err);
	res.status(500).send({ error:err.code||500, reason:err.reason||err.toString() });
	res.end();
});                     

var server = http.createServer(app);       // Express server with app
server.listen(app.get('port'), function() {// Listen and wait connections
	console.log('Using', db , 'DB with driver:', cfg.get(db + '.driver'));
	envcfg.load(function(err) { if(err) process.exit(1); });
	console.log('Loaded configuration: ' + cfg.get('name'));
	console.log('TutorIDE listening on http://' + app.get('host') + ':' + app.get('port'));
});
reload(server, app, 1500);                 // Reload for development support
