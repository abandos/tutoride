echo off
echo Please wait patiently because:
echo.
echo Containers should be created: be aware that Oracle takes veeery long to set up initial DB
echo Next time, it will take less
echo If oracle fails to pull, it's your fault - you need to run 'build-images' before!
echo.
echo To change DB model, cd to 'docker' and issue:
echo   $ ln -sf env_dev_couchdb .env # to start with CouchDB
echo   $ ln -sf env_dev_mongodb .env # to start with MongoDB
echo.
echo Please wait until web_1 is ready
cd docker
docker-compose up
