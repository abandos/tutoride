var fs = require('fs');

function fsExists(file) {
	try { fs.accessSync(file, fs.F_OK); }
	catch(err) { return false; }
	return true;
}

function readFileSyncIfExists(dir, file) {
	if(fsExists(dir + '/' + file))
		return fs.readFileSync(dir + '/' + file);
	return "";
}

module.exports = {
	fsExists: fsExists,
	readFileSyncIfExists: readFileSyncIfExists,
	isWritableDir: function (dir) {
		if(!fsExists(dir)) { console.log(dir + " does not exist"); return false; }
		if(!fs.statSync(dir).isDirectory()) { console.log(dir + " not directory"); return false; }
		try { fs.accessSync(dir, fs.W_OK); }
		catch(err) { console.log(dir + " not writable"); return false; }
		return true;
	},
	mkdirSyncIfNotExists: function (dir) {
		if(!fsExists(dir))
			fs.mkdirSync(dir);
	},
	cleanDirSync: function(dir) {
		var files = fs.readdirSync(dir);
		console.log("Deleting files: " + files.join(', '));
		for(i in files)
			fs.unlinkSync(dir + '/' + files[i]);
	},
	writeProjectFileSync: function(project, file, contents) {
		fs.writeFileSync(project + '/' + file, contents);
	},
	jsonizeOutput: function(output, csss) {
		// Return always a valid JSON file so we can see what's happening
		var obj = {}
		if(output.substr(0, 1) !== '{' && output.substr(1, 1) !== '{') { // Not a JSON
			obj = { "cmdResult":output.replace(/</g, '&lt;')
																.replace(/>/g, '&gt;')
																.replace(/\n*$/,'') };
		}
		else { // Maybe JSON
			try { // Try to parse, but don't die if not valid
				if(output.substr(0, 17) === '{"repType":"text"') // Text repr JSON
					obj = { "cmdResult":JSON.parse(output).text };
				else {
					obj = { "cmdGraph":JSON.parse(output), "csss":csss };
				}
			}
			catch(err) {
				console.log("JSON parse ERROR: ", err);
				obj = { "cmdResult":output + "JSON parse ERROR: " + err.message };
			}
		}
		return obj;
	}
};
