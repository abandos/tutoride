var cfg = require('config');
var db = require('../../' + cfg.get(cfg.get('db') + '.driver'));

module.exports = Object.assign(db(), { generateRandomPass:generateRandomPass });

function generateRandomPass() {
	var alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	var pass = "";
	var length = alphabet.length - 1;

	for(i = 0; i < 8; i++) {
		var n = (Math.random() * (alphabet.length - 1)).toFixed(0);
		pass += alphabet[n];
	}

	return pass;
}
