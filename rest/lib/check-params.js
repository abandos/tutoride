var u    = require('../lib/fs-util')
	, d    = require('../lib/db-util');

function check(valid, msg, code, reason) {
	if(valid) return;
	var err = new Error(msg);
	err.code = code;
	err.reason = reason;
	throw err;
};

module.exports = {
  check: check,
	json: function(req) {
		// Check content type
		check(req.accepts('json'), "Content not acceptable", 406,
			"JSON content required");
	},
	auth: function(req) {
		// Check authentication
		var session = req.session;
		check(session, "No session", 401, "Not authenticated or session expired");
	},
	optional: function(req, param) {
		// Check if param exists somewhere and return value
		return req.params[param] || req.body[param] || req.query[param];
	},
	required: function(req, param, unquote) {
		// Check param exists and return value (unquoted if true) if so
		var param = req.params[param] || req.body[param] || req.query[param];
		check(param, "Parameter [" + param + "] is required", 400,
			"Service contract violated");
		return unquote !== undefined && unquote ?
			param.slice(1, param.length - 1) : param;
	},
	allowed: function(req, param, list, modfunc) {
		// Check if param value is valid against a list of authorized params
		var value = modfunc ? modfunc(req.params[param]) : req.params[param];
		check(-1 < list.indexOf(value),
			"The value specified (" + value + ") for param " + param
			+ " is not one of: [" + list.join(", ") + "]", 400,
			"Service contract violated");
		return value;
	},
	usabledir: function(base, dir, name, make) {
		// Check first workDIR exists and is writable
		var fullname = base + '/' + dir;
		if(make) u.mkdirSyncIfNotExists(fullname);
		check(u.isWritableDir(fullname), 
			name + " '" + dir + "' not found, not a dir or not writeable!", 500,
			name + " in server is faulty. Please, contact your administrator");
		return fullname;
	},
	rescode: function(code, res, msg, jparse) {
		check(code == res.statusCode, msg + res.body, res.statusCode,
			res.body.toString());
		return jparse !== undefined && jparse ? JSON.parse(res.body) : res.body;
	},
	useracc: function(req, res, user, sub, msg, cb) {
		d.getSessionInfo(req, function(error, info) {
			if(user !== info.userCtx.name) {
				console.error("ACCESS DENIED: User " + info.userCtx.name + " tried to " + sub + "!");
				res.status(406).send({ "error":406, "reason":msg });
			}
			else cb();
		});
	}
};
