'use strict';

require('fs')
	.readdirSync(__dirname)
	.forEach(f => {
		if(f.endsWith('.js') && f !== 'index.js') {
			//let e = require(`./${ f }`);
			//module.exports = Object.assign({ [e.name]:e }, module.exports);
			module.exports = Object.assign(require(`./${ f }`), module.exports);
		}
	});
