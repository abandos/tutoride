'use strict';

const Api = require('kubernetes-client');
const core = new Api.Core(Api.config.fromKubeconfig()); //getInCluster());
const ext = new Api.Extensions(Api.config.fromKubeconfig());

// Non-class private funtions

function print(err, result) {
	console.log(JSON.stringify(err || result, null, 2));
}

function printPods(err, result) {
	if(err) return console.log(JSON.stringify(err), null, 2);
	console.log("NAME", "CREATION", "RS", "CONTAINER-NAME", "CONTAINER-PORT", "STATUS");
	result.items.forEach(p => {
		let name = p.metadata.name;
		let creation = p.metadata.creationTimestamp;
		let rs = p.metadata.ownerReferences[0].name;
		let cname = p.spec.containers[0].name;
		let cport = p.spec.containers[0].ports[0].containerPort
		let st = p.status.phase;
		console.log(`${name} ${creation} ${rs} ${cname} ${cport} ${st}`);
	});
}

class KuberHelper {
	constructor(options) {
		this.options = options;
	}

	printPods() {
		core.ns.pods.get(printPods);
	}

	printServices() {
		core.ns.svc.get(print);
	}

	printDeployments() {
		ext.ns.deployments.get(print);
	}
	
	printResourcesets() {
		ext.ns.rs.get(print);
	}
}

module.exports = { KuberHelper };
