var h = require('../docker/helper')
	, u = require('util');

module.exports = {
	mkdir: function(c, project, cb) {
		h.executeCont(c, u.format("mkdir -p '%s'", project), null, true, cb);
	},
	push: function(c, path, contents, cb) {
		h.executeCont(c, u.format("cat > '%s'", path), contents, true, cb);
	},
	pull: function(c, path, cb) {
		h.executeCont(c, u.format("cat '%s'", path), null, false, cb);
	},
	dir: function(c, path, cb) {
		h.executeCont(c, u.format("dir() { local IFS=\"\"; local f=0; echo \"{\\\"id\\\":$id,\\\"path\\\":\\\"$1\\\",\\\"title\\\":\\\"`basename $1`\\\",\\\"nodes\\\":[\"; id=$((id + 1)); for i in $1/*; do [ \"$i\" = \"$1/*\" ] && continue; echo -n \"$2  \"; [ $f != 0 ] && echo -n \",\"; f=1; if [ -d \"$i\" ]; then dir $i \"$2  \"; echo \"$2  ]}\"; else echo \"{\\\"id\\\":$id,\\\"path\\\":\\\"$i\\\",\\\"title\\\":\\\"`basename $i`\\\"}\"; id=$((id + 1)); fi; done }; id=1; dir '%s'; echo \"]}\"", path), null, false, cb);
	},
	rmdir: function(c, path, cb) {
		h.executeCont(c, u.format("rm -rf '%s'", path), null, true, cb);
	},
	rm: function(c, path, cb) {
		h.executeCont(c, u.format("rm -f '%s'", path), null, true, cb);
	},
	chown: function(c, uid, path, cb) {
		h.executeCont(c, u.format("chown -R %s '%s'", uid, path), null, true, cb);
	}
};
