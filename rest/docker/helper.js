var cfg      = require('config')
	, Docker   = require('dockerode')
	, Readable = require('stream').Readable
	, concat   = require('concat-stream')
	, tar      = require('tar-stream')
	, u        = require('util')
	, restutil = require('../lib/fs-util');

var daemon = cfg.get('rest.docker.daemon')
	, docker = new Docker(daemon);

console.log("Using docker daemon: " + JSON.stringify(daemon));

module.exports = {
	listCont: listCont,
	listNets: listNets,
	check: check,
	getCont: getCont,
	runInterp: runInterp,
	createCont: createCont,
	removeCont: removeCont,
	executeCont: executeCont,
	putArchive: putArchive,
	pipeArchive: pipeArchive
};

function listCont(callback) {
	docker.listImages({}, callback);
}

function listNets(callback) {
	docker.listNetworks({}, callback);
}

function check(container, callback) {
	container.inspect({}, callback);
}

function getCont(email, image) {
	var from = ['@',':','/'];
	var to = ['_at_','_colon_','_slash_'];
	return docker.getContainer((email + '--' + image).split('')
		.map(l => -1 < from.indexOf(l) ? to[from.indexOf(l)] : l).join(''));
}

function runInterp(opts, res) {
	var command = opts.cmd.slice(1, -1); // Command is always quoted
	var workdir = environs[opts.env].shells[opts.interp].workdir;
	var image = environs[opts.env].shells[opts.interp].image;
	var commandLine = [ "cd",  u.format("'%s/%s'", opts.env, opts.prj), "&&" ]
		.concat(environs[opts.env].shells[opts.interp].command
		.map(e => { return e.replace(/@a/g, command).replace(/\$/g, "\\$") })).join(' ');
	var cont = getCont(opts.db, image);
	executeCont(cont, commandLine, null, true, function(err, out) {
		if(err) {
			if(err.statusCode) {
				console.error("ERROR(runInterp):", err.json);
				return res.status(err.statusCode)
					.json({ error:err.statusCode, reason:err.json });
			}
			return res.send(restutil.jsonizeOutput(out ? out : err, null));
		}
		var path = workdir + '/' + opts.env + '/' + opts.prj + '/styles.css';
		executeCont(cont, u.format("cat '%s'", path), null, false,
			function(err, csss) {
				// csss might not be found, not an error
				console.log("executeCont+css", out);
				res.send(restutil.jsonizeOutput(out, csss));
			}
		);
	});
}

function createCont(cid, image, workdir, net, cb) {
	console.log("Creating container", cid, "with workdir", workdir);
	docker.createContainer({ name:cid, AttachStdout:true, AttachStderr:true,
		Tty:false, Cmd:["sh", "-c", "sleep " + cfg.get('rest.docker.cttl')],
		Image:image, WorkingDir:workdir, NetworkDisabled:false,
		Ulimits:[
			{ Name:"core",       Hard:0      , Soft:0 }, // blocks
			// { Name:"cpu",        Hard:30     , Soft:30 }, // seconds
			// { Name:"data",       Hard:1310712, Soft:98304 }, // KB
			//{ Name:"fsize",      Hard:8192000, Soft:8192000 }, // KB
			//{ Name:"locks",      Hard:512    , Soft:512 }, // files
			// { Name:"memlock",    Hard:64     , Soft:64 }, // KB
			//{ Name:"msgqueue",   Hard:819200 , Soft:819200 }, // Bytes
			//{ Name:"nice",       Hard:0      , Soft:0 },
			//{ Name:"nofile",     Hard:4096   , Soft:1024 }, // files
			// { Name:"nproc",      Hard:1024   , Soft:512 },
			//{ Name:"rss",        Hard:0      , Soft:0 },
			//{ Name:"rtprio",     Hard:0      , Soft:0 },
			//{ Name:"rttime",     Hard:0      , Soft:0 },
			//{ Name:"sigpending", Hard:31474  , Soft:31474 }, // signals
			//{ Name:"stack",      Hard:8192   , Soft:8192 }  // KB
 		], NetworkMode:net }, cb);
}

function removeCont(cid, cb) {
	var container = docker.getContainer(cid);
	container.remove({}, cb);
}

function executeCont(container, commandLine, contents, privileged, callback) {
	container.start({}, function(err, data) {
		if(err && err.statusCode !== 304)
			return callback(err, "in container.start");
		var cmd = ["sh", "-c", commandLine];
		var uid = privileged ? "0:0" : cfg.get("rest.docker.uid");
		var exopts = { Cmd:cmd, User:uid, AttachStdout:true,
			AttachStderr:true, AttachStdin:true };
		console.log("executeCont: " + cmd);
		container.exec(exopts, function(err, exec) {
			if(err) return callback(err, "in container.exec");
			exec.start({ hijack:true, stdin:true }, function(err, stream) {
				if(err) return callback(err, "in exec.start");
				var out = "", err = "";
				var stdout = { write:function(payload) { out += payload } };
				var stderr = { write:function(payload) { out += payload } }; // Mix out+err
				contents = contents ? contents : ""; // Prevent stdin input lock
				if(contents) {
					var r = new Readable;
					r.push(contents);
					r.push(null);
					r.pipe(stream);
				}
				stream.on('end', () => callback(err, out));
				docker.modem.demuxStream(stream, stdout, stderr);
			});
		});
	});
}

function putArchive(container, contents, path, uid, callback) {
	var pack = tar.pack();
	for(i in contents) {
		var e = contents[i];
		pack.entry({ name:e.name, type:e.type }, e.content);
	}
	pack.finalize();
	pack.pipe(concat(function(data) {
		container.putArchive(data, { path:path }, function(err, data) {
			if(err) return callback(err, data);
			executeCont(container, u.format("chown -R %s '%s'", uid, path), null,
				true, callback);
		});
	}));
}

function pipeArchive(srcCont, srcPath, tgtCont, tgtPath, uid, callback) {
	srcCont.getArchive({ path:srcPath }, function(err, src) {
		if(err) return callback(err, null);
		tgtCont.putArchive(src, { path:tgtPath }, function(err, dst) {
			if(err) return callback(err, null);
			executeCont(tgtCont, u.format("chown -R %s '%s'", uid, tgtPath), null,
				true, callback);
		});
	});
}
