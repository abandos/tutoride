var express = require('express')
var router  = express.Router()
var files = require('fs').readdirSync(__dirname + '/srv')

for(i in files) {
	var r = files[i]
	if(!r.endsWith('.js')) continue;
	var n = r.slice(0, -3)
	var resource = require('./srv/' + r);
	router[resource.method](resource.uri, resource.handler); 
}

module.exports = router;
