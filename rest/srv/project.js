var cfg   = require('config')
	, dockr = require('../docker/helper')
	, shell = require('../docker/shell')
	, check = require('../lib/check-params')
	, r     = require('../lib/db-util');

module.exports = {
	uri:"/project/:env/:proj?",
	method:"all",
	handler:handler
};

function handler(req, res) {
	check.auth(req);
	var env = check.required(req, 'env');
	var email = check.required(req, 'userdb');
	var interp = check.required(req, 'interp');
	var project = check.optional(req, 'proj');
	var image = environs[env].shells[interp].image;
	var workdir = environs[env].shells[interp].workdir;
	var net = environs[env].shells[interp].net;
	var container = dockr.getCont(email, image);
	var path = workdir + '/' + env + (project ? "/" + project : "");
	var template = check.optional(req, 'template');
	console.log("srv-project: serving", req.method, "to", req.url);

	if(req.method === 'GET')
		getProject(res, container, image, net, workdir, path);
	if(req.method === 'PUT')
		getProject(res, container, image, net, workdir, path, template, env, req);
	if(req.method === 'DELETE')
		removeProject(res, container, path);
}

function logErr(res, where, err) {
	var code = err.statusCode ? err.statusCode : 500;
	var msg = err.json ? err.json : err.toString();
	res.status(code).json({ error:code, reason:msg });
	console.error("ERROR in", where + ":", msg);
}

function getProject(res, container, image, net, workdir, path, template, env,
	req) {
	var processFiles = function(dir, filelist, cb) {
		if(!filelist.length) return cb();
		var file = filelist.pop();
		var filepath = path + "/" + file.d + "/" + file.special + "." + file.ext;
		shell.pull(container, filepath, function(err, out) {
			if(err) return logErr(res, "shell.pull", err);
			else {
				if(file.d == ".") dir[file.special] = out;
				else dir.nodes.find(e => e.title == file.d)[file.special] = out;
				return processFiles(dir, filelist, cb);
			}
		});
	};
	var addSpecial = function(d, special, ext, files) {
		if(!d.nodes || !d.nodes.length) {// addProject, no listing
			if(d.title == special + "." + ext)
				files.push({ d:".", special:special, ext:ext });
		}
		else {
			if(-1 !== d.nodes.findIndex(e => e.title == special + "." + ext))
				files.push({ d:d.title, special:special, ext:ext });
		}
	};
	var dir = function (container, path) {
		shell.dir(container, path, function(err, out) {
			if(err) return logErr(res, "shell.dir", err);
			else {
				var dir = JSON.parse(out);
				files = [];
				for(var i in dir.nodes) {
					var d = dir.nodes[i];
					addSpecial(d, "readme", "md", files);
					addSpecial(d, "macros" , "json", files);
				}
				processFiles(dir, files, function() {
					//console.dir(dir, {depth:null});
					res.json(dir);
				});
			}
		});
	};
	var mdir = function (tgtcont, tgtpath) {
		// Don't hurt and don't fail
		shell.mkdir(tgtcont, tgtpath, function(err, out) {
			if(err) return logErr(res, "mkdir", err);
			// Put template contents into new project dir if creating project
			if(!template) return dir(tgtcont, tgtpath);
			// Get template specification from templates DB
			r.getTemplates(env, req, function(err, resp) {
				if(err) return logErr(resp, "getTemplates", err);
				var tmpl = resp[template];
				var uid = cfg.get('rest.docker.uid');
				if("static" === tmpl.type) {
					dockr.putArchive(tgtcont, tmpl.contents, tgtpath, uid,
						function(err, data) {
							if(err) return logErr(res, "dockr.putArchive", err);
							return dir(tgtcont, tgtpath);
					});
				}
				else { // type === "live"
					var image = environs[env].shells[tmpl.interp].image;
					var workdir = environs[env].shells[tmpl.interp].workdir;
					var srccont = dockr.getCont(tmpl.owner, image);
					var srcpath = workdir + '/' + env + '/' + tmpl.project + '/.';
					var net = environs[env].shells[tmpl.interp].net;
					dockr.pipeArchive(srccont, srcpath, tgtcont, tgtpath, uid,
						function(err, data) {
							if(err) return logErr(res, "pipeArchive", err);
							console.log("Live template xferred successfully");
							return dir(tgtcont, tgtpath);
					});
				}
			});
		});
	};
	// TODO: User is allowed to create image (i.e. has this env)
	dockr.check(container, function(err, data) {
		if(err && err.statusCode !== 404) return logErr(res, "dockr.check", err);
		if(!err) return mdir(container, path);
		console.log("User container not found:", err.statusCode);
		dockr.createCont(container.id, image, workdir, net, function(err, data) {
			if(err) return logErr(res, "dockr.createCont", err);
			console.log("Creating env path and listing contents");
			mdir(container, path);
		});
	});
}

function removeProject(res, container, path) {
	shell.rmdir(container, path, function(err, out) {
		if(err) return logErr(res, "shell.rmdir", err);
		console.log("Project '" + path + "' removed successfully");
		res.json({ ok:true });
	});
}
