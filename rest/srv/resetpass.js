var cfg    = require('config')
	, mailer = require('nodemailer')
	, check  = require('../lib/check-params')
	, r      = require('../lib/db-util');

module.exports = {
	uri:"/resetpass",
	method:"put",
	handler:handler
};

function handler(req, res) {
	var resp = null; // Response of all HTTP external calls
	check.json(req); // Check correct content type
	// Prevent admin user from asking for a password reset
	var email = req.body.email;
	check.check("admin@tutoride.com" != email,
		"User admin@tutoride.com tried to reset his password!", 406,
		"Sorry, admin server cannot reset his password");
	// Get user data and check if fullname matches provided name
	var name = req.body.name;
	r.getUserInfo(email, name, function(err, userdata) {
		if(err) return res.status(err.error).json(err);
		// Generate and change password and send it only if succeeds
		// - make password change in the DB
		userdata.password = r.generateRandomPass(); // Send new password user details
		r.changePass(userdata, function(err, data) {
			if(err) return res.status(err.error).json(err);
			console.log('Password changed in the db for user ' + name);
			// - send email with new password to the email recipient
			mailer.createTransport(cfg.get('rest.resetpass.transport')).sendMail({
				from: cfg.get('rest.resetpass.from'),
				to: email,
				subject: "Restablecimiento de contraseña en TutorIDE",
				text: cfg.get('rest.resetpass.msgtemplate').join('\n')
					.replace('@@name@@', name).replace('@@email@@', email)
					.replace('@@newpass@@', userdata.password)
			}, function(error, info) {
				if(error) {
					console.log('sending email: ' + error);
					res.status(500).send({ "error": 500, "reason": error.toString() });
				}
				else {
					console.log('Email from', info.envelope.from, 'to',
						info.envelope.to, 'sent:', info.response);
					res.status(200).send("Envío correcto");
				}
			});
		});
	});
}
