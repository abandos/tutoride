var dockr = require('../docker/helper')
	, check = require('../lib/check-params');

module.exports = {
	uri:"/net",
	method:"all",
	handler:handler
};

function handler(req, res) {
	console.log("srv-net: serving", req.method, "to", req.url);
	check.auth(req); // Check authentication
	check.json(req); // Check content type
	check.useracc(req, res, "admin@tutoride.com", // Check authorization
		"list networks", "Sorry, only admin user can list docker networks",
		function() {
			if(req.method === "GET") listNetworks(res)
		}
	);
}

function listNetworks(res) {
	dockr.listNets(function(err, data) {
		if(err)
			return res.status(500).send({ "error":500, "reason":err.toString() });
		res.send({ total_rows: data.length, rows: data.map(e => e.Name) });
	});
}
