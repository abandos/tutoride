var mparty = require('multiparty')
	, fs     = require('fs')
	, dockr  = require('../docker/helper')
	, shell  = require('../docker/shell')
	, check  = require('../lib/check-params')
	, cfg    = require('config');

module.exports = {
	uri:"/file/:env",
	method:"all",
	handler:handler
};

function handler(req, res) {
	var email = req.query.userdb || req.body.userdb;
	var path = req.params.path || req.body.path || req.query.path;
	var isfile = req.params.isfile || req.body.isfile || req.query.isfile === "true";
	var content = req.body.content;
	var env = req.params.env || req.body.env;
	var interp = req.query.interp || req.body.interp;
	var image = environs[env].shells[interp].image;
	var container = dockr.getCont(email, image);
	console.log("srv-file: serving", req.method, "to", req.url);

	if(req.method === 'GET') getFile(container, path, res);
	if(req.method === 'POST') {
		if(content) putFile(container, path, content, isfile, res);
		else {
			var form = new mparty.Form({uploadDir:cfg.get("upload.dir")});
			form.parse(req, function(err, fields, files) {
				if(!files || !files.file[0] || !files.file[0].path) {
					res.status(500).json({ error:500, reason:"Invalid or null form upload file" });
					console.error("ERROR in POST handler:", "File data null or invalid");
					return;
				}
				var filename = files.file[0].path;
				console.log("Reading uploaded file:", filename);
				content = fs.readFileSync(filename);
				putFile(container, path, content, isfile, res);
			});
		}
	}
	if(req.method === 'DELETE') deleteFile(container, path, isfile, res);
}

function getFile(container, path, res) {
	shell.pull(container, path, function(err, out) {
		if(err) {
			res.status(500).json({ error:500, reason:err.toString() });
			console.error("ERROR in shell.pull:", err.toString());
		} else res.send(out);
	});
}

function putFile(container, path, contents, isfile, res) {
	if(isfile)
		shell.push(container, path, contents, function(err, out) {
			if(err) {
				res.status(500).json({ error:500, reason:err.toString() });
				console.error("ERROR in shell.push:", err.toString());
			} else res.json({ ok:true });
		});
	else
		shell.mkdir(container, path, function(err, out) {
			if(err) {
				res.status(500).json({ error:500, reason:err.toString() });
				console.error("ERROR in shell.mkdir:", err.toString());
			} else res.json({ ok:true });
		});
}

function deleteFile(container, path, isfile, res) {
	if(isfile)
		shell.rm(container, path, function(err, out) {
			if(err) {
				res.status(500).json({ error:500, reason:err.toString() });
				console.error("ERROR in shell.rm:", err.toString());
			} else res.json({ ok:true });
		});
	else
		shell.rmdir(container, path, function(err, out) {
			if(err) {
				res.status(500).json({ error:500, reason:err.toString() });
				console.error("ERROR in shell.rmdir:", err.toString());
			} else res.json({ ok:true });
		});
}
