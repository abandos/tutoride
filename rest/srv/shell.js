var cfg    = require('config')
	, dockr  = require('../docker/helper')
	, check  = require('../lib/check-params');

module.exports = {
	uri:"/shell/:interp",
	method:"post",
	handler:handler
};

function handler(req, res) {
	console.log("srv-shell: serving", req.method, "to", req.url);
	check.auth(req); // Check authentication
	check.json(req); // Check content type
	var opts = {
		db:check.required(req, 'database'), prj:check.required(req, 'project'),
		env:check.required(req, 'env'), cmd:check.required(req, 'command'),
		interp:check.required(req, 'interp') }; // Check required params
	// Check authorization to the interpreter
	check.allowed(req, 'interp', Object.keys(environs[opts.env].shells));
	// Check if command is allowed: authorized interpreter internal command
	if(":" === opts.cmd.substr(0, 1))
		check.allowed(req, 'command', environs[opts.env].allowed,
			function(s) { return s.split(" ")[0] });
	dockr.runInterp(opts, res);
}
