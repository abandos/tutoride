var dockr = require('../docker/helper')
	, check = require('../lib/check-params');

module.exports = {
	uri:"/container/:cid?",
	method:"all",
	handler:handler
};

function handler(req, res) {
	console.log("srv-container: serving", req.method, "to", req.url);
	check.auth(req); // Check authentication
	check.json(req); // Check content type
	check.useracc(req, res, "admin@tutoride.com", // Check authorization
		"manage containers", "Sorry, only admin user can list containers",
		function() {
			if(req.method === "GET") listContainers(res);
			if(req.method === "PUT") {
				var cid = check.required(req, 'cid');
				var image = check.required(req, 'image');
				var workdir = check.required(req, 'workdir');
				var net = check.required(req, 'net');
				createContainer(cid, image, workdir, net, res);
			}
			if(req.method === "DELETE") {
				var cid = check.required(req, 'cid');
				removeContainer(cid, res);
			}
		}
	);
}

function listContainers(res) {
	dockr.listCont(function(err, data) {
		if(err)
			return res.status(500).send({ "error":500, "reason":err.toString() });
		res.send({ total_rows: data.length, rows: data.map(e => e.RepoTags ?
			e.RepoTags[0] : e.RepoDigests[0].split('@')[0]) });
	});
}

function createContainer(cid, image, workdir, net, res) {
	console.log("Creating container '" + cid + " with workdir '" + workdir + "'");
	dockr.createCont(cid, image, workdir, net, function(err, data) {
		if(err) return res.status(err.statusCode)
			.json({ "error":err.statusCode, "reason":err.json });
		res.status(201).json({ ok:true, id:cid });
	});
}

function removeContainer(cid, res) {
	console.log("Deleting container:", cid);
	dockr.removeCont(cid, function(err, data) {
		if(err) return res.status(err.statusCode)
			.json({ "error":err.statusCode, "reason":err.json });
		console.log("Container " + cid + " destroyed");
		res.json({ ok:true, id:cid });
	});
}
