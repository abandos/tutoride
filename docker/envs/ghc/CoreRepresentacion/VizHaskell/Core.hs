{-# LANGUAGE FlexibleContexts #-}

{-|
Module      : VizHaskell.Core
Description : Definition of VizHaskell's main classes and @render@ function.

This module defines the main classes for defining representation types and
representable datatypes.
-}
module VizHaskell.Core(
          -- * Representation types
          Representation,
          -- * Representable values
          RPair,
          rPairRepresentation,
          rPairValue,
          buildRPair,
          -- * Representable types
          VizRepresentable(..),
          -- * Representation function
          render
      ) where

import Data.Text.Lazy.Builder
import qualified Data.Text.Lazy as T
import Data.Aeson.Encode
import Data.Aeson

{-|
  An empty class with no methods. Only those types denoting a representation
  type (such as string representation, tree representation, etc.) may be
  instances of this class
-}
class Representation rep

{-|
  It associates a representable value @a@ with its representation type @rep@.
  -}
data RPair rep a = RPair rep a

{-|
  Representation type
-}
rPairRepresentation :: RPair rep a -> rep
rPairRepresentation (RPair rep _) = rep

{-|
   Value to be represented
-}
rPairValue :: RPair rep a -> a
rPairValue (RPair _ a) = a

{-|
   It builds a representable value
-}
buildRPair :: Representation rep => rep -> a -> RPair rep a
buildRPair = RPair


{-|
  This class characterizes all the types which can be shown by VizHaskell's
  web interface.
-}
class VizRepresentable a where
  {-|
    It returns a JSON object containing the representation of
    the given value
  -}
  vizToJSON :: a -> Value



{-|
  This is the main function called by VizHaskell's web interface, and it
  returns a JSON representation of the value passed as second parameter, by
  using the representation type given by the first parameter.
-}
render :: (Representation rep, VizRepresentable (RPair rep a)) => rep -> a -> String
render rep x = T.unpack (toLazyText (encodeToTextBuilder (vizToJSON (RPair rep x))))

