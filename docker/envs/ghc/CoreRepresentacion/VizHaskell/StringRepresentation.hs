{-# LANGUAGE FlexibleInstances, OverloadedStrings #-}

{-|
Module      : VizHaskell.StringRepresentation
Description : Representation of all the types implementing @Show@.

This module defines the simplest representation kind: a string representation.
It defines the corresponding 'RepresentationString' type, which is an
instance of 'VizHaskell.Core.Representation'. It also specifies that all
instances of 'Show' must also be instances of 'RepresentationString'.
-}
module VizHaskell.StringRepresentation(RepresentationString(..)) where

import VizHaskell.Core
import Data.Aeson
import Data.String


{-|
  A representation type, which specifies that the value associated with
  this representation (see 'VizHaskell.Core.RPair') must be shown as plain text.
-}
data RepresentationString = RepresentationString

{-|
  'RepresentationString' is a representation type
-}
instance Representation RepresentationString

{-|
  This specifies that all the types implementing the 'Show' class can
  be represented with 'RepresentationString'
-}
instance Show a => VizRepresentable (RPair RepresentationString a) where
  vizToJSON rs = object [ "text" .= show (rPairValue rs) , "repType" .= String (fromString "text") ]
