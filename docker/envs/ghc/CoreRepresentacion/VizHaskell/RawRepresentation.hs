{-# LANGUAGE FlexibleInstances, OverloadedStrings, FlexibleContexts #-}

{-|
Module      : VizHaskell.RawRepresentation
Description : Special representation of raw JSON data

This module defines the 'RawRepresentation' class, with allows one to use
a data type that already is representable with a valid JSON contract
-}
module VizHaskell.RawRepresentation(RepresentationRaw(..)) where

import VizHaskell.Core
import Data.Aeson
import Data.Aeson.Types(Pair)
import Data.String
import qualified Data.Text
import qualified Data.Vector as V

data RepresentationRaw = RepresentationRaw

instance Representation RepresentationRaw

instance ToJSON a => VizRepresentable (RPair RepresentationRaw a) where
  vizToJSON rpair = toJSON (rPairValue rpair)
{-|
  The raw representable object is supposed to implement toJSON, so no other
  kind of processing is needed but to call it.
-}
