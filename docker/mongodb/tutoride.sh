#!/bin/sh
mongoimport -u admin@tutoride.com -p admin123 -d tutoride -c config /docker-entrypoint-initdb.d/mongodb-config-collection.json
mongoimport -u admin@tutoride.com -p admin123 -d tutoride -c templates /docker-entrypoint-initdb.d/mongodb-templates-collection.json
