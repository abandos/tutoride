// Create replicaSet (need root user or not auth)
db = db.getSiblingDB("admin")
db.runCommand({ replSetInitiate:{ _id:"rs0", members:[{ _id:0, host:"ha_mongo_1:27017", priority:10 }, { _id:1, host:"ha_mongo_2:27017" }, { _id:2, host:"ha_mongo_3:27017" }], settings:{ chainingAllowed:true } } })

