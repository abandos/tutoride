// Connect/create tutoride DB
db = db.getSiblingDB('tutoride')
// Create collection action roles
db.createRole({role:"admin.config", privileges:[{resource:{db:"tutoride",collection:"config"}, actions:["find", "update", "remove", "insert"]}], roles:[]})
db.createRole({role:"read.config", privileges:[{resource:{db:"tutoride",collection:"config"}, actions:["find"]}], roles:[]})
db.createRole({role:"read.templates", privileges:[{resource:{db:"tutoride",collection:"templates"}, actions:["find"]}], roles:[]})
// Create application roles
db.createRole({role:"learner", privileges:[{resource:{db:"tutoride", collection:""}, actions:["changeOwnPassword"]}], roles:["read.config", "read.templates"]})
db.createRole({role:"tutor", privileges:[{resource:{db:"tutoride",collection:""}, actions:["viewUser", "changePassword", "changeCustomData"]}], roles:["admin.config", "learner"]})
// Add admin@tutoride.com user with application roles and admin roles
db.createUser({user:"admin@tutoride.com", pwd:"admin123", roles:[{role:"dbOwner", db:"tutoride", isBuiltin:true}, {role:"tutor", db:"tutoride"}], customData:{envs:["ghc", "lua", "python", "sql"], name:"admin", fullname:"Admin TutorIDE"}})
