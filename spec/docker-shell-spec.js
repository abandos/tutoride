describe("Docker shell helper", function() {
	var Docker  = require('dockerode')
		, shell   = require('../rest/docker-shell');

  beforeEach(function() {
		dockr = new Docker({ host:'localhost', port:4243 });
		c = dockr.getContainer('robarago_at_gmail.com--lua-5.1.5_colon_latest');
		fileContents = "This is\na multiline\nfile\n";
	});

  afterEach(function() {
		//console.log("End of test suite");
	});

	it("should be able to create a project or folder", function(done) {
		shell.rmdir(c, "/home/user/lua", function(err, out) {
			expect(err).toBeFalsy();
			expect(out).toBeFalsy();
			shell.mkdir(c, "/home/user/lua/Proyecto 1/bt", function(err, out) {
				expect(err).toBeFalsy();
				expect(out).toBeFalsy();
				done();
			});
		});
	});

	it("should be able to create a file", function(done) {
		shell.push(c, "/home/user/lua/Proyecto 1/bt/bintree.lua", fileContents,
			function(err, out) {
				expect(err).toBeFalsy();
				expect(out).toBeFalsy();
				done();
			}
		);
	});

	it("should be able to read a file", function(done) {
		shell.pull(c, "/home/user/lua/Proyecto 1/bt/bintree.lua",
			function(err, out) {
				expect(err).toBeFalsy();
				expect(out).toEqual(fileContents);
				done();
			}
		);
	});

	it("should be able to list all project contents", function(done) {
		var text = "{\"type\":\"directory\",\"name\":\"Proyecto 1\",\"contents\":[\n  {\"type\":\"directory\",\"name\":\"bt\",\"contents\":[\n    {\"type\":\"file\",\"name\":\"bintree.lua\"}\n  ]}\n]}\n";
		shell.dir(c, "/home/user/lua/Proyecto 1", function(err, out) {
			expect(err).toBeFalsy();
			expect(out).toEqual(text);
			done();
		});
	});

	it("should be able to remove a full project or folder", function(done) {
		shell.rmdir(c, "/home/user/lua/Proyecto 1", function(err, out) {
			expect(err).toBeFalsy();
			expect(out).toBeFalsy();
			done();
		});
	});

	it("should return empty contents if a project or folder does not exist",
		function(done) {
		var output = "{\"type\":\"directory\",\"name\":\"Proyecto 1\",\"contents\":[\n]}\n";
		shell.dir(c, "/home/user/lua/Proyecto 1", function(err, out) {
			expect(err).toBeFalsy();
			expect(out).toEqual(output);
			done();
		});
	});
});
