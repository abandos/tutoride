var fs     = require('fs')
	, tar    = require('tar-stream')
	, concat = require('concat-stream')
	, Docker = require('dockerode');

var docker = new Docker({ host:'localhost', port:4243 });
//var container = docker.getContainer('robarago_at_gmail.com--lua-5.1.5_colon_latest');
var container = docker.getContainer('admiring_wescoff');

var pack = tar.pack();
// Put folders to container
// pack.entry({ name: "lua", type: "directory" })
// Put files into folder of container
pack.entry({
	name: "lua",
	type: "directory",
	mode: 0755,
	uid: 11,
	gid: 0,
	uname: "operator",
	gname: "root"
});
pack.entry({
	name: "lua/Proyecto 2",
	type: "directory",
	mode: 0755,
	uid: 11,
	gid: 0,
	uname: "operator",
	gname: "root"
});
pack.entry({
	name: "lua/Proyecto 1",
	type: "directory",
	mode: 0755,
	uid: 11,
	gid: 0,
	uname: "operator",
	gname: "root"
});
pack.entry({
	name: "lua/Proyecto 2/fichero2.txt",
	mode: 0644,
	uid: 11,
	gid: 0,
	uname: "operator",
	gname: "root"
}, "Contenido del fichero 2");
pack.entry({
	name: "lua/Proyecto 1/fichero1.txt",
	mode: 0666,
	uid: 11,
	gid: 0,
	uname: "operator",
	gname: "root"
}, "Contenido del fichero 1");
pack.finalize();
// Extract an archive of files into a directory in a container: path must exist
console.log("Putting data on /home/user");
pack.pipe(concat(function(data) {
	fs.writeFileSync('archive.tar', data);
	//container.putArchive(data, { path:"/home/user" }, function(err, stream) {
	container.putArchive(data, { User:"1000:1000", path:"/tmp" }, function(err, stream) {
		if(err) console.log(err);
		retrieveDir();
	});
}));

// Get directory contents from container
// path:"/."
// Get directory from container
// path:"/"
function retrieveDir() {
	console.log("Retrieving projects for lua from /home/user");
	//container.getArchive({ path:'/home/user/lua/' }, function(err, stream) {
	container.getArchive({ path:'/tmp/' }, function(err, stream) {
		if(err) return console.log(err);
		var extract = tar.extract();
		extract.on('entry', function(header, stream, callback) {
			console.log("Found entry:", header.name);
			stream.on('end', function() { callback() });
			stream.resume();
		});
		extract.on('finish', function() {
			console.log('All entries read');
			retrieveFile();
		});
		stream.pipe(extract);
	});
}
// Get file from container
function retrieveFile() {
	console.log("Retrieving file fichero.txt from /home/user/lua/Proyecto 2");
	//container.getArchive({ path:'/home/user/lua/Proyecto 2/fichero2.txt' }, function(err, stream) {
	container.getArchive({ path:'/tmp/lua/Proyecto 2/fichero2.txt' }, function(err, stream) {
		if(err) return console.log(err);
		var extract = tar.extract();
		extract.on('entry', function(header, stream, callback) {
			console.log("Open file:", header.name);
			stream.on('data', function(data) {
				console.log("Data:<<<");
				console.log(data.toString());
				console.log(">>>");
			});
			stream.on('end', function() {
				console.log("Close file:", header.name);
				callback();
			});
			stream.resume();
		});
		extract.on('finish', function() {
			console.log('All entries read');
			//removeAll();
		});
		stream.pipe(extract);
	});
}

/*
function changeAll() {
	container.start
}
*/

// Remove all (trick: overwrite dir with non-dir)
function removeAll() {
	var pack = tar.pack();
	pack.entry({ name: "lua", type: "file" }, "");
	pack.finalize();
	// Extract an archive of files into a directory in a container: path must exist
	console.log("Removing lua environment on /home/user");
	pack.pipe(concat(function(data) {
		container.putArchive(data, { path:"/home/user" }, function(err, stream) {
			if(err) console.log(err);
		});
	}));
}
