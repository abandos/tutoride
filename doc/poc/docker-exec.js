#!/usr/bin/env node

var fs       = require('fs')
	, Docker   = require('dockerode')
	, Readable = require('stream').Readable;

var docker = new Docker({ host:'localhost', port:4243 });
var container = docker
	.getContainer('robarago_at_gmail.com--lua-5.1.5_colon_latest');
var contents = fs.readFileSync(__dirname + '/bintree.lua');

var cmds = [
	{ cmd:"mkdir -p '/home/user/lua/Proyecto 1'" },           // Create project
	// Add file
	{ cmd:"cat > '/home/user/lua/Proyecto 1/bintree.lua'", input:contents },
	{ cmd:"cat '/home/user/lua/Proyecto 1/bintree.lua'" },          // Get file
	{ cmd:"dir() { local IFS=\"\"; local f=0; echo \"{\\\"type\\\":\\\"directory\\\",\\\"name\\\":\\\"`basename $1`\\\",\\\"contents\\\":[\"; for i in $1/*; do [ \"$i\" == \"$1/*\" ] && continue; echo -n \"$2  \"; [ $f != 0 ] && echo -n \",\"; f=1; if [ -d \"$i\" ]; then dir $i \"$2  \"; echo \"$2  ]}\"; else echo \"{\\\"type\\\":\\\"file\\\",\\\"name\\\":\\\"`basename $i`\\\"}\"; fi; done }; dir '/home/user/lua/Proyecto 1'; echo \"]}\"" }              // List project contents
//	{ cmd:"rm -rf '/home/user/lua/Proyecto 1'" },                // Delete project
//	{ cmd:"ls -l '/home/user/lua/Proyecto 1'" }                      // Get errors
];

// Start tests
test(0);

function test(testNum) {
	execOnCont(cmds[testNum].cmd, cmds[testNum].input, function(err, out) {
		if(err) console.log("Errors from CMD#" + testNum + ": <<<" + err + ">>>");
		if(out) console.log("Output from CMD#" + testNum + ": <<<" + out + ">>>");
		if(cmds[testNum + 1]) test(testNum + 1);
	});
}

function execOnCont(commandLine, contents, callback) {
	container.start({}, function(err, data) {
		if(err && err.statusCode !== 304)
			return console.log("container.start:", err);
		var cmd = ["sh", "-c", commandLine];
		var exopts = { Cmd:cmd, AttachStdout:true,
			AttachStderr:true, AttachStdin:true };
		console.log("Executing command: " + cmd.join(' '));
		container.exec(exopts, function(err, exec) {
			if(err) return console.log("container.exec:", err);
			exec.start({ hijack:true, stdin:true }, function(err, stream) {
				if(err) return console.log("container.start:", err);
				var out = "", err = "";
				var stdout = { write:function(payload) { out += payload } };
				var stderr = { write:function(payload) { err += payload } };
				if(contents) {
					var r = new Readable;
					r.push(contents);
					r.push(null);
					r.pipe(stream);
				}
				stream.on('end', () => callback(err, out));
				docker.modem.demuxStream(stream, stdout, stderr);
			});
		});
	});
}
