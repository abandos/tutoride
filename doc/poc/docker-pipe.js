var fs     = require('fs')
	, tar    = require('tar-stream')
	, concat = require('concat-stream')
	, Docker = require('dockerode');

var docker = new Docker({ host:'localhost', port:4243 });
var src = docker.getContainer('admin_at_tutoride.com--lua-5.1.5_colon_latest');
var tgt = docker.getContainer('robarago_at_gmail.com--lua-5.1.5_colon_latest');

function pipeArchive() {
	src.getArchive({ path:'/home/user/lua/Lua 101-01' },
	function(err, src) {
		if(err) return console.log(err);
		tgt.putArchive(src, { User:"1000:1000", path:"/home/user/lua" },
		function(err, dst) {
			if(err) console.log(err);
		});
	});
}

pipeArchive();
