var files = require('fs').readdirSync(__dirname + '/lib')
for(i in files) {
	var r = files[i]
	var n = r.slice(0, -3)
	module.exports[n] = require('./lib/' + r)[n]
}
