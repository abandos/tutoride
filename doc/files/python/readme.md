# Bintree

This is a binary tree implementation with a representation
function complying with the rules of the JSON contract
for graphic display of data structures in D3.

Try:

```python
>>> print(test1)
```

With `test1` to `test4` to show how different trees
are rendered, included a tree inside a tree (test2).

Good luck.
