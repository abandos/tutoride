def render(elem):
  if isinstance(elem, bintree):
  	return elem.__str__()
  else:
  	return '{"repType":"text","text":"' + elem.__str__() + '"}'

class bintree:
  def __init__(self, name=None, left=None, right=None):
    self._left = left
    self._name = name
    self._right = right

  def __str__(self):
    return '{"repType":"tree","value":'\
  		+ render(self._name) + ',"children":['\
    	+ ','.join(render(x) for x in [self._left, self._right] if x)\
      	+ "]}"

test1 = bintree(0,bintree(1),bintree(2))
test2 = bintree(bintree(0))
test3 = bintree()
test4 = bintree(1)
