#!/usr/bin/env python
'''
Python module
'''

def square(n):
    """
    Returns the squared n
    """
    return n * n
