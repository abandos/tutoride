
module BinarySearchTree where

import VizHaskell.TreeRepresentation
import VizHaskell.StringRepresentation
import VizHaskell.Core

data BSTree a = Empty | Node (BSTree a) a (BSTree a)
  deriving (Show, Eq, Ord)

instance TreeRepresentable BSTree where
  contents Empty        = Nothing
  contents (Node _ x _) = Just x

  children  Empty               = []
  children (Node Empty x Empty) = []
  children (Node l x Empty)     = [l]
  children (Node Empty x r)     = [r]
  children (Node l x r)         = [l,r]



insert :: Ord a => a -> BSTree a -> BSTree a
insert x Empty = Node Empty x Empty
insert x (Node l y r)
  | x == y = Node l y r
  | x <  y = Node (insert x l) y r
  | x >  y = Node l y (insert x r)


deleteMin :: BSTree a -> (a,BSTree a)
deleteMin (Node Empty x r) = (x,r)
deleteMin (Node l x r) = (y, Node l' x r)
  where (y, l') = deleteMin l


delete :: Ord a => a -> BSTree a -> BSTree a
delete x Empty = Empty
delete x (Node l y r)
  | x < y = Node (delete x l) y r
  | x > y = Node l y (delete x r)
  | x == y = join l r


join :: BSTree a -> BSTree a -> BSTree a
join Empty r     = r
join l     Empty = l
join l     r     = Node l x r'
  where (x, r') = deleteMin r

test1 :: BSTree Int
test1 = foldr insert Empty [10,9,3,1,4,5,6]

test2 :: BSTree Char
test2 = foldr insert Empty "Hello, world"


test3 :: BSTree (BSTree Int)
test3 = foldr insert Empty [insert 0 Empty, insert 1 Empty, insert 2 Empty]

{- Try this:

putStrLn $ render RepresentationString test1
putStrLn $ render RepresentationString test2
putStrLn $ render (RepresentationTree RepresentationString) test1
putStrLn $ render (RepresentationTree RepresentationString) test2
-}
