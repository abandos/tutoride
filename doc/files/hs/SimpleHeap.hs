{-# LANGUAGE OverloadedStrings #-}
module SimpleHeap where

import Data.Aeson
import Data.Aeson.Types(Pair)
import qualified Data.Vector as V

data SimpleHeap = None | SimpleHeap { left :: SimpleHeap, val :: Int, right :: SimpleHeap }

instance Show SimpleHeap where
  show (None) = "None"
  show (SimpleHeap i v d) = "<" ++ show i ++ "|" ++ show v ++ "|" ++ show d ++ ">"

instance ToJSON SimpleHeap where
  toJSON (SimpleHeap None v None) = object ([] ++ addV v ++ addL v)
  toJSON (SimpleHeap i v None ) = object ([ "children" .= [i] ] ++ addV v ++ addL v)
  toJSON (SimpleHeap None v d ) = object ([ "children" .= [d] ] ++ addV v ++ addL v)
  toJSON (SimpleHeap i v d ) = object ([ "children" .= [i, d] ] ++ addV v ++ addL v)

addV :: Int -> [Pair]
addV v = ["value" .= object [ "text" .= (show v) ]] -- value text must be text
addL :: Int -> [Pair]
addL v = ["label" .= ("Node " ++ show v)]
            
sh1 :: SimpleHeap
sh1 = SimpleHeap (SimpleHeap None 11 None) 0 $ SimpleHeap None 2 $ SimpleHeap None 20 None

{-
Try this:

putStrLn $ render RepresentationString sh1
putStrLn $ render RepresentationRaw sh1
putStrLn $ unpack $ encode sh1
-}
