{-# LANGUAGE DeriveGeneric #-}

module Heap where

import Data.Aeson
import GHC.Generics

data Heap = Empty | Node Heap Int Heap deriving (Show, Generic)

instance ToJSON Heap
