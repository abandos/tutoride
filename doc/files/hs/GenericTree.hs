{-# LANGUAGE DeriveGeneric #-}

module GenericTree where

import GHC.Generics
import Data.Char (toLower)
import Data.Aeson

data GenericTree = Leaf String | Branch String [ GenericTree ] deriving (Show, Generic)

instance ToJSON GenericTree
