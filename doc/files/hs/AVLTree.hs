
module AVLTree where

import VizHaskell.TreeRepresentation
import VizHaskell.StringRepresentation
import VizHaskell.Core

data AVLTree a = Empty | Node (AVLTree a) a (AVLTree a) Int
  deriving Show

instance TreeRepresentable AVLTree where
  contents Empty          = Nothing
  contents (Node _ x _ _) = Just x

  children  Empty                 = []
  children (Node Empty x Empty _) = []
  children (Node l x Empty _)     = [l]
  children (Node Empty x r _)     = [r]
  children (Node l x r _)         = [l,r]

  label Empty          = Just ""
  label (Node _ _ _ h) = Just (show h)


height :: AVLTree a -> Int
height Empty = 0
height (Node _ _ _ h) = h


node :: AVLTree a -> a -> AVLTree a -> AVLTree a
node l x r = Node l x r (1 + max (height l) (height r))

bias :: AVLTree a -> Int
bias (Node l _ r _) = height r - height l

rotR :: AVLTree a -> AVLTree a
rotR (Node (Node ll y lr _) x r _) = node ll y (node lr x r)

rotL :: AVLTree a -> AVLTree a
rotL (Node l x (Node rl y rr _) _) = node (node l x rl) y rr

join :: AVLTree a -> a -> AVLTree a -> AVLTree a
join l x r
  | abs (hl - hr) <= 1          = node l x r
  | hr == hl - 2 && bias l <= 0 = rotR (node l x r)
  | hr == hl - 2 && bias l >  0 = rotR (node (rotL l) x r)
  | hl == hr - 2 && bias r >= 0 = rotL (node l x r)
  | hl == hr - 2 && bias r <  0 = rotL (node l x (rotR r))
  where hl = height l
        hr = height r



insert :: Ord a => a -> AVLTree a -> AVLTree a
insert x Empty = node Empty x Empty
insert x n@(Node l y r _)
  | x <  y = join (insert x l) y r
  | x >  y = join l y (insert x r)
  | x == y = n

delete :: Ord a => a -> AVLTree a -> AVLTree a
delete x Empty = Empty
delete x n@(Node l y r _)
  | x <  y = join (delete x l) y r
  | x >  y = join l y (delete x r)
  | x == y = merge l r



deleteMin :: AVLTree a -> (a, AVLTree a)
deleteMin (Node Empty x r _) = (x, r)
deleteMin (Node l x r _) = (y, join l' x r)
  where (y, l') = deleteMin l

merge :: AVLTree a -> AVLTree a -> AVLTree a
merge Empty r = r
merge l Empty = l
merge l r = join l x r'
  where (x, r') = deleteMin r


test1 = foldr insert Empty [1,2,3,4,5,6,7,8]
test2 = foldr delete test1 [3,7]

{- Try this:

putStrLn $ render RepresentationString test1
putStrLn $ render RepresentationString test2
putStrLn $ render (RepresentationTree RepresentationString) test1
putStrLn $ render (RepresentationTree RepresentationString) test2
-}
