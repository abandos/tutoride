{-# LANGUAGE OverloadedStrings #-}
module IntTree where

import Data.Aeson
import Data.Aeson.Types(Pair)

data IntTree = Empty | IntTree
  { left :: IntTree,
     val :: Int,
   right :: IntTree } deriving (Show)

instance ToJSON IntTree where
  toJSON (IntTree i v d ) = case (i, d) of
    (Empty, Empty) -> addValue [] v
    (Empty,     d) -> addValue [d] v
    (i    , Empty) -> addValue [i] v
    (i    ,     d) -> addValue [i, d] v
    where addValue :: [IntTree] -> Int -> Value
          addValue p v = object [
            "repType" .= String "tree",
            "children" .= p,
            "value" .= object [
              "repType" .= String "text",
              "text" .= (show v)
                ],
            "label" .= ("Node " ++ show v)
            ]

it1 :: IntTree
it1 = IntTree (IntTree Empty 1 Empty) 2 (IntTree Empty 3 Empty)

{-
Try this:

putStrLn $ render RepresentationString it1
putStrLn $ render RepresentationRaw it1
putStrLn $ unpack $ encode it1
-}
