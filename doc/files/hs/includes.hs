-- Funciones necesarias para la salida de datos
import Data.Aeson                         -- (encode)
import Data.ByteString.Lazy.Char8(unpack) -- (unpack)
import VizHaskell.TreeRepresentation      -- (tree contract)
import VizHaskell.StringRepresentation    -- (simple string)
import VizHaskell.RawRepresentation       -- (raw JSON contract)
import VizHaskell.Core                    -- (support lib)
