module BinTree where

import Data.String
import VizHaskell.TreeRepresentation(TreeRepresentable(..))

data BSTree a = Empty | Node (BSTree a) a (BSTree a)
  deriving (Show, Eq, Ord)

instance TreeRepresentable BSTree where
  contents Empty        = Nothing
  contents (Node _ x _) = Just x

  children  Empty               = []
  children (Node Empty x Empty) = []
  children (Node l x Empty)     = [l]
  children (Node Empty x r)     = [r]
  children (Node l x r)         = [l,r]
  
  className Empty = Nothing
  className (Node l _ Empty) = Just "negra"
  className (Node Empty _ r) = Just "roja"
  className (Node _ _ _) = Just "azul"

insert :: Ord a => a -> BSTree a -> BSTree a
insert x Empty = Node Empty x Empty
insert x (Node l y r)
  | x == y = Node l y r
  | x <  y = Node (insert x l) y r
  | x >  y = Node l y (insert x r)

test1 :: BSTree Int
test1 = foldr insert Empty [10,9,3,1,4,5,6]

test2 :: BSTree Char
test2 = foldr insert Empty "Hello, World"

test3 :: BSTree (BSTree Int)
test3 = foldr insert Empty [
  test1,
  foldr insert Empty [7, 2, 8],
  foldr insert Empty [10, 30, 20, 40]
    ]

test4 :: BSTree (BSTree Char)
test4 = foldr insert Empty [test2, foldr insert Empty "What's up?"]

{-
Examples of use:

putStrLn $ render RepresentationString test1
putStrLn $ render (RepresentationTree RepresentationString) test1
putStrLn $ render RepresentationString test2
putStrLn $ render (RepresentationTree RepresentationString) test2
putStrLn $ render RepresentationString test3
putStrLn $ render (RepresentationTree (RepresentationTree RepresentationString)) test3
putStrLn $ render RepresentationString test4
putStrLn $ render (RepresentationTree (RepresentationTree RepresentationString)) test4
-}
