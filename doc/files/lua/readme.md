# BinTree

Simple implementation of a binary
tree in [Lua](http://lua.org).

## Interpreters

You can change the interpreter
using the command:

```
lua> :iset luaInt
```

**luaExe**
	For evaluating project and lua system functions, but not for simple
	expresions.

**luaInt**
	Full coverage of simple expresions and evaluation of project
	functions.

Try

```lua
print(test1)
```

- `test1`: three elements binary tree
- `test2`: tree (of one only node)
  inside a one only node tree
- `test3`: tree with only one node

For more information, please see
the [5.3 version manual
online](https://www.lua.org/manual/5.3/)
(CTRL+Click).

