BinTree = {}
BinTree.mt = {}

function BinTree.new(t)
	local tree = {
    	left = t.left,
    	name = t.name,
    	right = t.right
  	}
	setmetatable(tree, BinTree.mt)
	return tree
end

BinTree.mt.__tostring = function(t)
  local s = '{"repType":"tree","children":['
  if t.left == nil then
    s = s .. ''
  else
    s = s .. BinTree.mt.__tostring(t.left)
  end
  if t.right == nil then
    s = s .. ']'
  else
    s = s .. ',' .. BinTree.mt.__tostring(t.right) .. ']'
  end
  if t.name then
    if type(t.name) == "string" or type(t.name) == "number" then
  		s = s .. ',"value":{"repType":"text","text":"' .. t.name .. '"}'
    elseif type(t.name) == "table" then
		s = s .. ',"value":' .. BinTree.mt.__tostring(t.name)
    end
  else
		s = s .. ',"value":{"repType":"text","text":"None"}'
  end
  return s .. '}\n'
end

test1 = BinTree.new{
  left = BinTree.new{
    left = nil,
    name = 1,
    right = nil
  },
  name = 0,
  right = BinTree.new{
    left = nil,
    name = 2,
    right = nil
  }
}

test2 = BinTree.new{name = BinTree.new{ name = 0 }}
test3 = BinTree.new{name = nil}
test4 = BinTree.new{name = 1}
