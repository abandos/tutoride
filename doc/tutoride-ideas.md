# IDEAS PARA TUTORIDE

## Uso de CP (copiar a contenedor)
Cambiar el repositorio primario de proyectos y 
ficheros que ahora está en la BD y usar únicamente 
el contenedor del usuario.

Al crear y subir ficheros, se utilizaría el contenedor
(que podría estar parado) usando la copia remota.
Desde la versión RAPI 1.20, se usa el servicio
"archive", soportado en dockerode como getArchive y
putArchive, que usan formato TAR intermedio.

### Pros:

* Soluciona el problema de separación docker-nodejs 
  (posible distribución de docker en máquina 
  diferente y/o uso de docker swarm)
* Fácil implementación de carpetas en proyectos
* Un único repositorio de ficheros
* Menor trasiego de datos, ya que no se mantienen
  ficheros en BD y en un directorio
* Se puede usar la consola sin un proyecto asociado
* Se requieren menos accesos y estructura en BD

### Contras:

* Requiere contenedor permanente (puede estar parado)
* Requiere módulo manejo fichero TAR
* Se requieren más accesos a docker
* Nueva operativa para el tratamiento de ficheros y
  carpetas: crear y subir archivos, crear carpetas,
  listar carpeta, bajar archivos.
* El usuario administrador require un contenedor por
  entorno (debe hacerse en el mantenimiento de entornos)
* Se complica el mantenimiento de usuarios: en alta se
  crean contenedores, en baja se eliminan, en actualización
  una mezcla de los dos.
* Cambia el modelo de proyectos y ficheros

### Pruebas realizadas

* Subir un proyecto completo:

      $ tar c Proyecto -f - | curl -vvv -XPUT 'http://localhost:4243/containers/<id>/archive?path=/home' -H 'Content-Type: application/octet-stream' --data-binary @-
 
* Listar el contenido de un proyecto (recursivamente):

      $ curl -vvv -XPOST 'http://localhost:4243/containers/<id>/exec' -H 'Content-Type: application/json' --data-binary '{ "AttachStdout":true, "AttachStderr":true, "Cmd": [ "ls", "/home/Proyecto" ] }
      # output:
      {"Id":"<exec_id"}
      $ curl -vvv -XPOST 'http://localhost:4243/exec/<exec_id>/start' -H 'Content-Type: application/json' --data-binary '{ "Dettach":false, "Tty":false }'

## Eliminación de VizHaskell

Estudiar un método más sencillo (y que no requiera instalación de paquetes externos, como VizHaskell)
para la conversión de instancias al JSON de representación, p.e. usando la conversión a String.
Ver cómo se ha hecho para LUA y hacer una prueba de concepto similar para BinTree y otros, ya que
así podríamos pasar a GHC 8 y librarnos de los problemas de compilación de VizHaskell.

## Uso de volúmenes

Cambiar el espacio volátil actual por espacio persistente usando alguna de estas opciones:

* Un volumen por usuario y entorno: al crear el contenedor de entorno-usuario, si no existe, como ahora
  se creará el volumen asociado. El espacio de usuario no requiere crear directorio de usuario ni entorno
  o, si se quiere, se puede dejar como ahora, se crea un directorio por entorno.
* Un volumen por usuario para todos los entornos: crearlo al crear el usuario o al crear el primer
  contenedor de alguno de sus entornos. El espacio de usuario tendría un directorio por entorno, igual que
  ahora, pero los usuarios podrían "ver" todos sus directorios de entorno en su espacio reservado.
* Un volumen /home por entorno con permisos por usuario: en este caso se requeriría un nuevo modelo de
  control de acceso (asignación de UID/GID por usuario) y la creación de directorio de usuario y, dentro
  de este, directorio de entorno por cada contenedor entorno-usuario que se cree, asignándole los
  permisos UNIX necesarios.

### Pros

* El almacenamiento ya sería persistente y, ante caídas o pérdidas de contenedores, se tendría mayor
seguridad.
* Los usuarios podrían utilizar ficheros comunes, compartir recursos, descargar sus datos...
* El acceso mejoraría, ya que no se usa UNIONFS ni OVERLAYFS
