var Docker = require('dockerode');
var docker = new Docker();
docker.run('docker.io/robarago/ghc-7.10.2', [
	'sh', '-c',
	'while [ "$d" != "`stat -c%X ../ghc.env`" ]; do d=`stat -c%X ../ghc.env`; sleep 60; done'],
	process.stdout, {
		name: "ghc-admin_tutoride.com", tty: false, readonlyrootfs: true, networkdisabled: true,
		user: "1000",
		binds: [
			"/tmp:/tmp",
			"/home/roberto/src/tutoride/work/admin%24tutoride_com:/work"],
		workingdir: "/work/BinTree"
	}, function(err, data, container) {
		container.remove({}, function(err, data) {
			if(!err) console.log("Container " + container.id + " finished and removed");
		});
	});
setTimeout(function() {
	docker.getContainer('ghc-admin_tutoride.com').exec({
		cmd: [ "sh", "-c", "echo \"test1\" | ghci -v0 includes.hs" ], attachstdout: true,
		attachstderr: true, stream: false
	}, function(err, exec) {
		if(err) console.log(err);
		else exec.start(function(err, stream) {
			if(err) console.log(err);
			else {
				//stream.on('data', function(data) { console.log(data.toString()) })
				stream.pipe(process.stdout);
			}
		})
	});
}, 500);
