## Migración API docker (dockerode) => kubernetes (kubernetes-client)

# docker
- docker.createContainer -> 
- docker.getContainer by name -> core.ns.pods('hellonode-7f4ff8fd69-pdzsg').get(cb)
- docker.getContainer by app -> core.ns.pods.matchLabels({ app:'hellonode' }).get(cb)
- docker.listImages -> deployments?
- docker.listNetworks -> namespaces?
- docker.modem.demuxStream

# container
- container.remove
- container.inspect
- container.start
- container.exec
- container.putArchive

# exec
- exec.start
