# Trabajo pendiente

1 Consola configurable avanzada y multilenguaje (módulo angular tutor-console)
  1.1 Lista de comandos internos (del intérprete) permitidos dependiendo de intérprete
  1.2 Macrocomandos (:) dependiendo de intérprete
  1.3 Ejercicios interactivos ???
  1.6 Ayuda
  1.8 Paquetización (cambiar bower)
  1.9 Markdown: la consola muestra descripciones textuales intercaladas
      con órdenes que, mediante un botón, puedan ejecutarse en el intérprete
      (facilita copiar/pegar típico, permite experimentar o incluso seguir
      tutoriales que requieren completar un paso para pasar al siguiente)

2 Video tutorial enriquecido
  2.1 Canales: 3 canales simultáneos
      1) Canal de edición de ficheros
			2) Canal de consola
			3) Canal de presentación/índice/pizarra (DDP/websocket)
  2.2 Funciones: tutoría online, tutorial diferido, asistencia online (editor,
      audio/video), editor vivo (captura remota).

4 Complección en CodeMirror

5 Límites de ejecución docker
  5.1 Prevención de fork bomb
  5.2 Prevención de escalado de privilegios

7 Integración con moodle: estudiar

8 Plantillas de proyectos
  8.3 También será posible crear un proyecto desde una plantilla albergada
      en GitHub (público).

9 Mejoras propuestas
	9.2 Persistencia de sesiones con MongoDB
	9.3 Internacionalización (consola y servidor)
  9.4 Revisión de seguridad (CORS, inyección de CSS y JS, etc)
  9.5 Logging (con morgan?)
	9.6 Clusterizar e independizar de BD (docker y/o node y/o BD/LDAP)
	9.8 HTTPs para express (fácil, tengo enlace) y/o para docker
	9.9 Pruebas del API REST con servicios externos
	9.10 Pruebas del cliente y del servidor jasmine

0 Errores pendientes
  0.5 Verificar si un usuario está habilitado para crear un contenedor
      (si tiene el entorno que intenta "abrir")
  0.6 Cambiar mkdir -p por mkdir, ya que no detecta errores: provoca un bug
      en la creación de proyectos, ya que puede duplicar entradas
  0.7 Cambiar mkdir por cd && mkdir para restringir el ámbito acción de las
      rutas
